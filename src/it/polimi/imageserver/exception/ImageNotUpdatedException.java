/**
 *
 */
package it.polimi.imageserver.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.ImageResponse;
import it.polimi.imageserver.response.UserResponse;

/**
 * Exceptions thrown by UserResource when a user is not created
 */
public class ImageNotUpdatedException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	private static final int errorCode = 4056;
	private static final String status = "FAIL";
	private static final String message = "Error during image updating";


	/**
	 * User not updated exception
	 * @param user the user that should be been updated
	 */
	public ImageNotUpdatedException(Image image) {
		super(Response.serverError().entity(new ImageResponse(errorCode, status, message, image))
				.type(MediaType.APPLICATION_JSON).build());
	}

}
