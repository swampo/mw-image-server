/**
 *
 */
package it.polimi.imageserver.exception;

/**
 * @author Andrea Salmoiraghi
 *
 */
public class DAOUserNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public DAOUserNotFoundException(String message){
      super(message);
   }
}
