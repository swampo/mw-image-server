/**
 *
 */
package it.polimi.imageserver.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.UserResponse;

/**
 * Exceptions thrown by UserResource when a user is not created
 */
public class UserNotUpdatedException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	private static final int errorCode = 4006;
	private static final String status = "FAIL";
	private static final String message = "Error during user updating";


	/**
	 * User not updated exception
	 * @param user the user that should be been updated
	 */
	public UserNotUpdatedException(User user) {
		super(Response.serverError().entity(new UserResponse(errorCode, status, message, user))
				.type(MediaType.APPLICATION_JSON).build());
	}

}
