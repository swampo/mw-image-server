/**
 *
 */
package it.polimi.imageserver.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.UserResponse;

/**
 * Exceptions thrown by when a user is not authorized to view the content
 */
public class UserNotAuthorizedException extends WebApplicationException {
   private static final long serialVersionUID = 1L;

   private static final String message = "User not authorized";

   /**
    * User not authorized exception
    */
   public UserNotAuthorizedException() {
      super(Response.status(Status.UNAUTHORIZED)
            .entity(message)
            .type(MediaType.APPLICATION_JSON)
            .build());
   }
}
