/**
 *
 */
package it.polimi.imageserver.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.UserResponse;

/**
 * Exceptions thrown by UserResource when a user is not created
 */
public class UserNotCreatedException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	private static final int errorCode = 4005;
	private static final String status = "FAIL";
	private static final String message = "Error during user creation";


	/**
	 * User not created exception
	 * @param user the user that should be been created
	 */
	public UserNotCreatedException(User user) {
		super(Response.serverError().entity(new UserResponse(errorCode, status, message, user))
				.type(MediaType.APPLICATION_JSON).build());
	}

}
