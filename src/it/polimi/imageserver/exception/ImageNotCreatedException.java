/**
 *
 */
package it.polimi.imageserver.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import it.polimi.imageserver.response.ImageResponse;


/**
 * Exceptions thrown by UserResource when a user is not created
 */
public class ImageNotCreatedException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	private static final int errorCode = 4055;
	private static final String status = "FAIL";
	private static final String message = "Error during image saving";


	/**
	 * User not created exception
	 * @param user the user that should be been created
	 */
	public ImageNotCreatedException() {
		super(Response.serverError().entity(new ImageResponse(errorCode, status, message))
				.type(MediaType.APPLICATION_JSON).build());
	}

}
