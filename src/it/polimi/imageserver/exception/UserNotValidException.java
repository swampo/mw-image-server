/**
 *
 */
package it.polimi.imageserver.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.UserResponse;

/**
 * Exceptions thrown by UserResource when a user validated
 */
public class UserNotValidException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	private static final int errorCode = 4013;
	private static final String status = "FAIL";
	private static final String message = "User not valid - ";

	/**
	 * User not valid exception
	 * @param user the user that should be validated
	 */
	public UserNotValidException(User user, String fieldMessage) {
		super(Response.serverError().entity(new UserResponse(errorCode, status, message + fieldMessage, user))
				.type(MediaType.APPLICATION_JSON).build());
	}

}
