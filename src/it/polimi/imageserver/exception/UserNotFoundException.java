/**
 *
 */
package it.polimi.imageserver.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.UserResponse;

/**
 * Exceptions thrown by UserResource when a user is not found
 */
public class UserNotFoundException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	private static final int errorCode = 4004;
	private static final String status = "FAIL";
	private static final String message = "User not found";


	/**
	 * @param user the user searched
	 */
	public UserNotFoundException() {
		super(Response.status(Status.NOT_FOUND).entity(new UserResponse(errorCode, status, message))
				.type(MediaType.APPLICATION_JSON).build());
	}

}
