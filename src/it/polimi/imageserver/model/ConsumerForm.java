/**
 *
 */
package it.polimi.imageserver.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.FormParam;

import org.junit.validator.ValidateWith;

import it.polimi.imageserver.website.model.EmailChecker;


/**
 * ConsumerForm bean
 */
public class ConsumerForm {
   @NotNull
   @ValidateWith(EmailChecker.class)
   @FormParam("email")
   protected String email;
   @NotNull
   @Size(min=2, max=50)
   @FormParam("name")
   protected String name;

   public ConsumerForm(){
   }

   public ConsumerForm(String name, String email) {
      this.name = name;
      this.email = email;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }
}
