package it.polimi.imageserver.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageContainer {

	private List<Image> images;
	private String uri;

	public ImageContainer() {
		super();
	}

	public ImageContainer(List<Image> images, String uri) {
		this.images = images;
		this.uri = uri;
	}
	
	public ImageContainer(Image[] images, String uri) {
		this.images = new ArrayList<Image>();
		for (Image i: images) {
			this.images.add(i);
		}
		this.uri = uri;
	}
	
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
}
