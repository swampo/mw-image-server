package it.polimi.imageserver.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserContainer {
	
	private List<User> users;

	public UserContainer() {
		this.users = new ArrayList<User>();
	}
	
	public UserContainer(List<User> users) {
		this.users = users;
	}
	
	public UserContainer(User[] users) {
		this.users = new ArrayList<User>();
		for(User u: users) {
			this.users.add(u);
		}
	}
	
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
}
