/**
 *
 */
package it.polimi.imageserver.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Image class
 *
 */
@Entity
@XmlRootElement
public class Image implements Serializable {
   private static final long serialVersionUID = 1L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO) // UID
   @XmlElement
   private String id;

   @Column(name = "title")
   @XmlElement
   private String title;
   @Column(name = "extension")
   @XmlElement
   private String extension;
   @Column(name = "caption")
   @XmlElement
   private String caption;
   @Column(name = "uri")
   @XmlElement
   private String uri;
   @Column(name = "created")
   @XmlElement
   private Timestamp created;
   @Column(name = "lastUpdate")
   @XmlElement
   private Timestamp lastUpdate;

   // @XmlInverseReference(mappedBy="images")
   private User owner;

   public Image() {}

   /**
    * To clone an image.
    *
    * @param user
    */
   public Image(Image image) {
      this.updateValues(image);
   }

   public Image(String id, User owner, String title, String extension) {
      this.id = id;
      this.owner = owner;
      this.title = title;
      this.extension = extension;
      this.created = new Timestamp(new Date().getTime());
      this.lastUpdate = this.created;
   }

   public Image(String id, User owner, String title, String extension, String caption) {
      this(id, owner, title, extension);
      this.caption = caption;
   }

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getExtension() {
      return extension;
   }

   public void setExtension(String extension) {
      this.extension = extension;
   }

   public String getCaption() {
      return caption;
   }

   public void setCaption(String caption) {
      this.caption = caption;
   }

   public String getUri() {
      return uri;
   }

   public void setUri(String uri) {
      this.uri = uri;
   }

   public Timestamp getCreated() {
      return created;
   }

   public void setCreated(Timestamp created) {
      this.created = created;
   }

   public Timestamp getLastUpdate() {
      return lastUpdate;
   }

   public void setLastUpdate(Timestamp lastUpdate) {
      this.lastUpdate = lastUpdate;
   }

   /*
    * (non-Javadoc)
    *
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object obj) {
      if (obj == null)
         return false;
      if (obj == this)
         return true;
      if (!(obj instanceof Image))
         return false;
      Image other = (Image) obj;
      if (other.id == null)
         return false;
      return id.equals(other.id);
   }

   @Override
   public String toString() {
      return "{ id: " + this.id + ", title: " + this.title + ", caption: " + this.caption + ", uri: " + this.uri + " }";
   }

   /**
    * Clone the user
    * Update each attribute of the object with the value of an other object (if not null)
    *
    * @param userNewValues
    * The user with new values
    */
   public void updateValues(Image imageNewValues) {
      if (imageNewValues == null) {
         return;
      }
      Field[] fields = this.getClass()
            .getDeclaredFields();
      for (Field field : fields) {
         try {
            Object value = field.get(imageNewValues);
            if (field.get(imageNewValues) != null) {
               field.set(this, value);
            }
         } catch (IllegalArgumentException | IllegalAccessException e) {
            // e.printStackTrace();
         }
      }
      return;
   }
}
