package it.polimi.imageserver.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * User class
 *
 */
@XmlRootElement
public class ConsumerBean {

   @XmlElement
   private String name;
   @XmlElement
   private String key;
   @XmlElement
   private String secret;

   public ConsumerBean() {}

   public ConsumerBean(String name, String key, String secret) {
      this.name = name;
      this.key = key;
      this.secret = secret;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getKey() {
      return key;
   }

   public void setKey(String key) {
      this.key = key;
   }

   public String getSecret() {
      return secret;
   }

   public void setSecret(String secret) {
      this.secret = secret;
   }

   @Override
   public String toString(){
      return "{ name: '" + name + "', key: '" + key + "', secret: '" + secret + "' }";
   }
}
