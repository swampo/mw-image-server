package it.polimi.imageserver.model;

import java.lang.reflect.Field;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * User class
 *
 */
@Entity
@NamedQuery(name = "getAllUsers", query = "SELECT u FROM User u")
@XmlRootElement
public class User implements Principal {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO) // UID from UserDAO
   @XmlElement
   private String id;

   @Column(name = "email")
   @XmlElement
   private String email;
   @Column(name = "password")
   @XmlElement
   private String password;

   @Column(name = "name")
   @XmlElement
   private String name;
   @Column(name = "uri")
   @XmlElement
   private String uri;
   @Column(name = "created")
   @XmlElement
   private Timestamp created;
   @Column(name = "lastUpdate")
   @XmlElement
   private Timestamp lastUpdate;

   @OneToMany(mappedBy = "owner", cascade = { CascadeType.ALL })
   private Map<String, Image> images = new HashMap<>();

   public User() {}

   public User(String name, String email, String password) {
      this.name = name;
      this.email = email;
      this.password = password;
      this.created = new Timestamp(new Date().getTime());
      this.lastUpdate = this.created;
   }

   /**
    * To clone an user.
    *
    * @param user
    */
   public User(User user) {
      this.updateValues(user);
   }

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getUri() {
      return uri;
   }

   public void setUri(String uri) {
      this.uri = uri;
   }

   public Timestamp getCreated() {
      return created;
   }

   public void setCreated(Timestamp created) {
      this.created = created;
   }

   public Timestamp getLastUpdate() {
      return lastUpdate;
   }

   public void setLastUpdate(Timestamp lastUpdate) {
      this.lastUpdate = lastUpdate;
   }

   public Set<Image> getImages() {
      return new HashSet<Image>(images.values());
   }

   public void addImage(Image image) {
      images.put(image.getId(), image);
   }

   public Image getImage(String id) {
      return images.get(id);
   }

   /**
    * Check if the user has an image
    *
    * @param imageId
    * @return
    */
   public boolean hasImage(String id) {
      return images.containsKey(id);
   }

   /*
    * (non-Javadoc)
    *
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object obj) {
      if (obj == null)
         return false;
      if (obj == this)
         return true;
      if (!(obj instanceof User))
         return false;
      User otherUser = (User) obj;
      if (otherUser.id == null)
         return false;
      return id.equals(otherUser.id);
   }

   @Override
   public String toString() {
      return "{ id: " + this.id + ", email: " + this.email + ", name: " + this.name + ", uri: " + this.uri + " }";
   }

   /**
    * Clone the user
    * Update each attribute of the object with the value of an other object (if not null)
    *
    * @param userNewValues
    * The user with new values
    */
   public void updateValues(User userNewValues) {
      if (userNewValues == null) {
         return;
      }
      Field[] fields = this.getClass()
            .getDeclaredFields();
      for (Field field : fields) {
         try {
            Object value = field.get(userNewValues);
            if (field.get(userNewValues) != null) {
               field.set(this, value);
            }
         } catch (IllegalArgumentException | IllegalAccessException e) {
            // e.printStackTrace();
         }
      }
      return;
   }
}
