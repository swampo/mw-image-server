package it.polimi.imageserver.storage;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import it.polimi.imageserver.database.DBStorage;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.model.UserContainer;

public class UserStorage {
	public static UserStorage US = new UserStorage();

	private List<User> users = new ArrayList<User>();

	public UserContainer getUserContainer() {
		if(users.isEmpty())
			return new UserContainer();
		return new UserContainer(users);
	}
	
	public boolean existUser(String userId) {
		User u = this.getUser(userId);
		return users.contains(u);
	}

	public User getUser(String userId) {
		for(User u: users) {
			if(u.getUserId().equals(userId))
				return u;
		}
		return null;
	}

	public void addUser(User user) {
		this.users.add(user);

//		String query = "INSERT INTO users (name, uniqueId, password) VALUES ('"+user.getName()+"','"+user.getUniqueId()+"','psw')";
//		try {
//			DBStorage.query(query);
//		} catch (NamingException e) {
//			e.printStackTrace();
//		}
	}
}
