package it.polimi.imageserver.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.ImageContainer;

public class ImageStorage {

	public static ImageStorage IS = new ImageStorage();
	
	private Map<String, String> userImages = new HashMap<>();
	private Map<String, String> imageTitles = new HashMap<>();
	private Map<String, byte[]> images = new HashMap<>();
	private String partialURI = "http://localhost:8080/ImageServer/rest/users/";
	private String imageContainerURI = "/imageContainer/";

	public ImageStorage() {
		super();
	}
	
	public byte[] getImage(String imageName) {
		return images.get(imageName);
	}
	
	public void addImage(String userId, String imageName, String imageTitle, byte[] b) {
		images.put(imageName, b);
		imageTitles.put(imageName, imageTitle);
		userImages.put(imageName, userId);
	}
	
	public void deleteImage(String imageName) {
		images.remove(imageName);
		imageTitles.remove(imageName);
		userImages.remove(imageName);
	}
	
	public boolean existsImage(String imageName) {
		return images.containsKey(imageName);
	}
	
	public ImageContainer getImageContainer(String userId) {
		if (images.keySet().isEmpty()) {
			return new ImageContainer();
		}
		List<String> userImageNames = new ArrayList<String>();
		Map<String,String> tmpUserImages = new HashMap<String,String>(this.userImages);
	    Iterator<Entry<String, String>> it = tmpUserImages.entrySet().iterator();
	    while (it.hasNext()) {
	        Entry<String, String> pair = it.next();
	        if(pair.getValue().equals(userId))
	        	userImageNames.add((String) pair.getKey());
	        it.remove();
	    }
//		Set<String> names = images.keySet();
		Image[] imageArray = new Image[userImageNames.size()];
		int i = 0;
		for (String imgName : userImageNames) {
			String imgTitle = imageTitles.get(imgName);
			imageArray[i] = new Image(imgName, imgTitle, partialURI+userId+imageContainerURI+imgName);
			i++;
		}
		System.out.println("storage->"+imageArray.length);
		return new ImageContainer(imageArray,partialURI+userId+imageContainerURI);
	}
	
	
	
	
	
}
