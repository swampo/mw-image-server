/**
 *
 */
package it.polimi.imageserver.dao;

import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Singleton;
import javax.ws.rs.core.UriBuilder;

import org.jvnet.hk2.annotations.Service;

import it.polimi.imageserver.exception.DAOUserNotFoundException;
import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.resource.UserResource;

/**
 * UserDAO implementation with generic hashMap where to store the users
 * (for testing)
 */
@Singleton
@Service
public class UserDAOmap implements UserDAO {

   private static Map<String, User> users = new HashMap<>();
   private static final UriBuilder baseUriBuilder = UriBuilder.fromResource(UserResource.class)
         .path("info");

   public UserDAOmap() {
      // Test DATA
      // Default user
      User defaultUser = new User("Default", "default@default.com", "Default");
      defaultUser.setId("u_default");
      URI userUri = baseUriBuilder.clone()
            .path(defaultUser.getId())
            .build();
      defaultUser.setUri(userUri.toString());
      defaultUser.setCreated(new Timestamp(new Date().getTime()));
      defaultUser.setLastUpdate(defaultUser.getCreated());
      users.put(defaultUser.getId(), defaultUser);
      // Add image
      Image image = new Image("testDownload", defaultUser, "batman", "png");
      defaultUser.addImage(image);
      // Others users
      int n = 5;
      int i;
      User user;
      String uid;
      for (i = 0; i < n; i++) {
         user = new User("User" + i, "user" + i + "@test.com", "User" + i);
         uid = "u_user" + i;
         user.setId(uid);
         userUri = baseUriBuilder.clone()
               .path(user.getId())
               .build();
         user.setUri(userUri.toString());
         user.setCreated(new Timestamp(new Date().getTime()));
         user.setLastUpdate(user.getCreated());
         users.put(user.getId(), user);
      }
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#create(it.polimi.imageserver.model.User)
    */
   @Override
   public User create(User user) throws Exception {
      if (user == null) {
         throw new Exception("User null");
      }
      String uid = UUID.randomUUID()
            .toString();
      user.setId(uid);
      URI userUri = baseUriBuilder.clone()
            .path(uid)
            .build();
      user.setUri(userUri.toString());
      user.setCreated(new Timestamp(new Date().getTime()));
      user.setLastUpdate(user.getCreated());
      users.put(user.getId(), user);
      if (!users.containsKey(user.getId())) {
         throw new Exception("User not created");
      }
      return user;
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#delete(long)
    */
   @Override
   public boolean delete(User user) {
      if (!exists(user)) {
         return false;
      }
      return users.remove(user.getId(), user);
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#update(it.polimi.imageserver.model.User)
    */
   @Override
   public User update(User user) throws Exception {
      if (user == null) {
         throw new Exception("User null");
      }
      if (!exists(user)) {
         throw new Exception("User not exists");
      }
      users.put(user.getId(), user);
      return user;
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#read(long)
    */
   @Override
   public User read(String id) {
      return users.get(id);
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#exists(long)
    */
   @Override
   public boolean exists(String id) {
      return users.containsKey(id);
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#exists(it.polimi.imageserver.model.User)
    */
   @Override
   public boolean exists(User user) {
      return users.containsValue(user);
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#getAll()
    */
   @Override
   public List<User> getAll() {
      List<User> usersList = new ArrayList<User>(users.values());
      return usersList;
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#getUserFromUsernamAndPassword(java.lang.String, java.lang.String)
    */
   @Override
   public User getUserFromUsernameAndPassword(String username, String password) throws DAOUserNotFoundException {
      List<User> users = getAll();
      for (User u : users) {
         if (u.getEmail().equals(username) && u.getPassword().equals(password)) {
            return u;
         }
      }
      throw new DAOUserNotFoundException("User with username " + username + " and password " + password + " not found");
   }

}
