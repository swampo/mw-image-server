/**
 *
 */
package it.polimi.imageserver.dao;

import java.util.List;

import org.jvnet.hk2.annotations.Contract;

import it.polimi.imageserver.exception.DAOUserNotFoundException;
import it.polimi.imageserver.model.User;

/**
 * User Data Access Object interface
 */
@Contract
public interface UserDAO {
	/**
	 * Store a new user assigning it the id and the uri. Throws exception if user is not created
	 *
	 * @param user
	 * @throws Exception
	 * @return the user with id and uri
	 */
	public User create(User user) throws Exception;

	/**
	 * Delete a user. Throws exception if user is found but not deleted
	 *
	 * @param user
	 * @return true if the user is deleted
	 */
	public boolean delete(User user);

	/**
	 * Update an user. Throws exception if user is found but not updated
	 *
	 * @param user
	 * @throws Exception
	 * @return the user updated
	 */
	public User update(User user) throws Exception;

	/**
	 * Find an user (Return null if user is not found)
	 *
	 * @param id
	 * @return user
	 */
	public User read(String id);

	/**
	 * Check if an user exist
	 *
	 * @param id
	 * @return true if an user with id provided exist
	 */
	public boolean exists(String id);

	/**
	 * Check if an user exist
	 *
	 * @param user
	 * @return true if the user exist
	 */
	public boolean exists(User user);

	/**
	 * Get all the users stored
	 * @return list of users
	 */
	public List<User> getAll();

	/**
	 * Get user searching using the username and the password
	 * @param username
	 * @param password
	 * @return the user found
	 * @throws UserNotFoundException if user is not found
	 */
	public User getUserFromUsernameAndPassword(String username, String password) throws DAOUserNotFoundException;

}
