/**
 *
 */
package it.polimi.imageserver.dao;

import java.net.URI;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.ws.rs.core.UriBuilder;

import org.jvnet.hk2.annotations.Service;

import it.polimi.imageserver.exception.DAOUserNotFoundException;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.resource.UserResource;

/**
 * UserDAO implementation with mysql db and persistence manager
 */
@Service
@Stateless
public class UserDAOdb implements UserDAO {

   private static final UriBuilder baseUriBuilder = UriBuilder.fromResource(UserResource.class)
         .path("info");

   /*
    * Jersey not support JavaEE-based functionality SPI not inject
    * EntityManager correctly (null) (...Works instead in resources) An entity
    * manager can only be injected in classes running inside a transaction. In
    * other words, it can only be injected in a EJB. So UserDAOdb must be a
    * bean and injected with @EJB (NOTE: this works only on server with EJB
    * support, like glassfish) In this way persistence and transactions are
    * automatically controlled by the container
    */
   @PersistenceContext(unitName = "mw_imageserver", type = PersistenceContextType.TRANSACTION)
   EntityManager em;

   /*
    * (non-Javadoc)
    *
    * @see
    * it.polimi.imageserver.dao.UserDAO#create(it.polimi.imageserver.model.
    * User)
    */
   @Override
   public User create(User user) throws Exception {
      if (user == null) {
         throw new Exception("User null");
      }
      String uid = UUID.randomUUID()
            .toString();
      user.setId(uid);
      URI userUri = baseUriBuilder.clone()
            .path(uid)
            .build();
      user.setUri(userUri.toString());
      user.setCreated(new Timestamp(new Date().getTime()));
      user.setLastUpdate(user.getCreated());
      em.persist(user);
      if (!em.contains(user)) {
         throw new Exception("User not created");
      }
      return user;
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#delete(long)
    */
   @Override
   public boolean delete(User user) {
      if (!exists(user)) {
         return false;
      }
      em.remove(user);
      if (!exists(user)) {
         return true;
      }
      return false;
   }

   /*
    * (non-Javadoc)
    *
    * @see
    * it.polimi.imageserver.dao.UserDAO#update(it.polimi.imageserver.model.
    * User)
    */
   @Override
   public User update(User user) throws Exception {
      if (user == null) {
         throw new Exception("User null");
      }
      if (!exists(user)) {
         throw new Exception("User not exists");
      }
      em.merge(user);
      return user;
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#read(long)
    */
   @Override
   public User read(String id) {
      return em.find(User.class, id);
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#exists(long)
    */
   @Override
   public boolean exists(String id) {
      if (em.find(User.class, id) != null) {
         return true;
      }
      return false;
   }

   /*
    * (non-Javadoc)
    *
    * @see
    * it.polimi.imageserver.dao.UserDAO#exists(it.polimi.imageserver.model.
    * User)
    */
   @Override
   public boolean exists(User user) {
      return exists(user.getId());
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#getAll()
    */
   @SuppressWarnings("unchecked")
   @Override
   public List<User> getAll() {
      Query query = em.createNamedQuery("getAllUsers");
      return (List<User>) query.getResultList();
   }

   /*
    * (non-Javadoc)
    *
    * @see it.polimi.imageserver.dao.UserDAO#getUserFromUsernamAndPassword(java.lang.String, java.lang.String)
    */
   @Override
   public User getUserFromUsernameAndPassword(String username, String password) throws DAOUserNotFoundException {
      try {
         User user = (User) em.createQuery("SELECT u FROM  User u WHERE u.email = :userEmail AND u.password = :userPassword")
               .setParameter("userEmail", username)
               .setParameter("userPassword", password)
               .setMaxResults(1)
               .getSingleResult();
         return user;
      } catch (Exception e) {
         throw new DAOUserNotFoundException("User with username " + username + " and password " + password + " not found");
      }
   }

}
