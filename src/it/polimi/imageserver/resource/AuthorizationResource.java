package it.polimi.imageserver.resource;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider;
import org.glassfish.jersey.server.oauth1.OAuth1Provider;
import org.glassfish.jersey.server.oauth1.TokenResource;

import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.exception.DAOUserNotFoundException;
import it.polimi.imageserver.exception.UserNotAuthorizedException;
import it.polimi.imageserver.model.User;
import jersey.repackaged.com.google.common.collect.Sets;

@Path("/authorize")
public class AuthorizationResource {
   @Context
   private OAuth1Provider provider;

   @Inject // For testing
   @EJB // For UserDAOdb (with Glassfish)
   UserDAO users;

   @TokenResource
   @GET
   public Response authorize(@QueryParam("oauth_token") String token, @QueryParam("user_email") String email,
         @QueryParam("user_password") String password) {
      if (token == null || email == null || password == null) {
         return Response.status(Status.BAD_REQUEST)
               .build();
      }
      // Check user exist
      try {
         User user = users.getUserFromUsernameAndPassword(email, password);
         // Return the verifier
         final DefaultOAuth1Provider defProvider = (DefaultOAuth1Provider) provider;

         String role = "user";
         final String verifier = defProvider.authorizeToken(defProvider.getRequestToken(token), user, Sets.newHashSet(role));
         return Response.ok(verifier)
               .build();
      } catch (DAOUserNotFoundException e) {
         throw new UserNotAuthorizedException();
      }
   }

   /**
    * Test resource online
    *
    * @return It Works
    */
   @Path("online")
   @GET
   public Response test() {
      return Response.ok("Authorization resource online")
            .build();
   }
}
