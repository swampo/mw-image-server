package it.polimi.imageserver.resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import com.sun.jersey.api.ConflictException;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import it.polimi.imageserver.model.ImageContainer;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.storage.ImageStorage;
import it.polimi.imageserver.storage.UserStorage;

public class ImageContainerResource {
		
	private String userId;
	private UriInfo uriInfo;
	
	public ImageContainerResource(String uniqueId, UriInfo uriInfo) {
		this.userId = uniqueId;
		this.uriInfo = uriInfo;
	}

	@GET
	@Produces("application/json")
	public Response getContainer() {
		ImageContainer ic = UserStorage.US.getUser(userId).getImageContainer();
		return Response.ok(ic).build();
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response postNewImage(
			@FormDataParam("file") InputStream fileStream, 
			@FormDataParam("file") FormDataContentDisposition fileDisposition,
			@FormDataParam("imageTitle") String title) {		
		String imageName = fileDisposition.getFileName();
		
		if(imageName==null || title==null)
			Response.status(Status.BAD_REQUEST).build();
		
		if(ImageStorage.IS.existsImage(imageName))
			throw new ConflictException("409 An image with the same id already exists.");

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		byte[] actualDataToStore = new byte[16384];
		int nBytesRead;
		
		try {
			while ((nBytesRead = fileStream.read(actualDataToStore, 0, actualDataToStore.length)) != -1) {
				buffer.write(actualDataToStore, 0, nBytesRead);
			}
			buffer.flush();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		//actual storage
		ImageStorage.IS.addImage(userId, imageName, title, buffer.toByteArray());
		User user = UserStorage.US.getUser(userId);
		user.setImageContainer(ImageStorage.IS.getImageContainer(userId));
		
		String encodedImageName = null;
		try {
			encodedImageName = URLEncoder.encode(imageName,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		URI uri = URI.create(uriInfo.getAbsolutePath()+encodedImageName);
		return Response.created(uri).build();
	}
	
	@Path("{imageName}")
	public ImageResource getImageResource(@PathParam(value = "imageName") String imageName) {
		return new ImageResource(imageName,uriInfo);
	}	
	
}
