package it.polimi.imageserver.resource;

import java.net.URI;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.validator.routines.EmailValidator;

import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.exception.UserNotCreatedException;
import it.polimi.imageserver.exception.UserNotFoundException;
import it.polimi.imageserver.exception.UserNotUpdatedException;
import it.polimi.imageserver.exception.UserNotValidException;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.UserResponse;
import it.polimi.imageserver.response.UsersListResponse;

/**
 * Resource for users management
 *
 */

@Path("/users")
public class UserResource {

   private final String mediaType = MediaType.APPLICATION_JSON;

   @Context
   UriInfo uriInfo;

   @Inject // For testing
   @EJB // For UserDAOdb (with Glassfish)
   UserDAO users;

   /**
    * Create a new user Assign ID and URI at the user created.
    *
    * @param user
    * @return confirm user creation
    */
   @POST
   @Consumes(mediaType)
   @Produces(mediaType)
   @Path("create")
   public Response create(@NotNull User user) {
      boolean valid = checkIsValidUser(user);
      // New user copied from the one received
      User userCreated = new User(user);
      try {
         // Save new user. Assign the id and uri
         userCreated = users.create(userCreated);
         // Set URI
         if (!userCreated.getUri()
               .startsWith("http")) {
            URI uri = uriInfo.getBaseUriBuilder()
                  .path(userCreated.getUri())
                  .build();
            userCreated.setUri(uri.toString());
         }
      } catch (Exception e) {
         // e.printStackTrace();
         throw new UserNotCreatedException(user);
      }
      UserResponse userResponse = new UserResponse("User created", userCreated);
      return Response.created(UriBuilder.fromUri(userCreated.getUri())
            .build())
            .entity(userResponse)
            .build();
   }

   /**
    * Check if the user is valid
    *
    * @param user
    */
   private boolean checkIsValidUser(User user) {
      if (user.getEmail() == null) {
         throw new UserNotValidException(user, "Email cannot be null");
      }
      if (user.getPassword() == null) {
         throw new UserNotValidException(user, "Password cannot be null");
      }
      if (user.getEmail()
            .equals("")) {
         throw new UserNotValidException(user, "Email cannot be empty");
      }
      EmailValidator ev = EmailValidator.getInstance();
      if (!ev.isValid(user.getEmail())) {
         throw new UserNotValidException(user, "Email is not valid");
      }
      return true;
   }

   /**
    * Get an user.
    *
    * @param id
    * The id of the user
    * @return the user if found or exception
    */
   @GET
   @Produces(mediaType)
   @Path("info/{id}")
   public Response read(@NotNull @PathParam("id") String id) {
      // Load the user
      User user = users.read(id);
      if (user == null) {
         throw new UserNotFoundException();
      }
      // Set URI
      if (!user.getUri()
            .startsWith("http")) {
         URI uri = uriInfo.getBaseUriBuilder()
               .path(user.getUri())
               .build();
         user.setUri(uri.toString());
      }
      UserResponse userResponse = new UserResponse("User found", user);
      return Response.ok()
            .entity(userResponse)
            .build();
   }

   /**
    * Update an existing user.
    * If not exist return the error
    *
    * @param user
    * @return the user updated or error
    */
   @PUT
   @Produces(mediaType)
   @Consumes(mediaType)
   @Path("update/{id}")
   public Response update(@NotNull @PathParam("id") String id, @NotNull User userNewValues) {
      // Search the user
      User user = users.read(id);
      if (user == null) {
         throw new UserNotFoundException();
      }
      // Backup the user in case of errors
      User bckUser = new User(user);
      // Update user and store it only at the end
      // Overwrite not editable fields
      userNewValues.setId(user.getId());
      userNewValues.setUri(user.getUri());
      userNewValues.setCreated(user.getCreated());
      // Update the user
      user.updateValues(userNewValues);
      // Set last update
      user.setLastUpdate(new Timestamp(new Date().getTime()));
      try {
         users.update(user);
         // Set URI
         if (!user.getUri()
               .startsWith("http")) {
            URI uri = uriInfo.getBaseUriBuilder()
                  .path(user.getUri())
                  .build();
            user.setUri(uri.toString());
         }
         UserResponse userResponse = new UserResponse("User updated", user);
         return Response.ok()
               .entity(userResponse)
               .build();
      } catch (Exception e) {
         // e.printStackTrace();
         // Reset the user to old values
         user.updateValues(bckUser);
         throw new UserNotUpdatedException(user);
      }
   }

   /**
    * Get all users
    *
    * @return list of all users
    */
   @GET
   @Produces(mediaType)
   @Path("/list")
   public Response getAllUser() {
      List<User> usersList = users.getAll();
      // Set URI
      for (User u : usersList) {
         if (!u.getUri()
               .startsWith("http")) {
            URI uri = uriInfo.getBaseUriBuilder()
                  .path(u.getUri())
                  .build();
            u.setUri(uri.toString());
         }
      }
      UsersListResponse usersListResponse = new UsersListResponse("Users list", usersList);
      return Response.ok()
            .entity(usersListResponse)
            .build();
   }

   /**
    * Test resource online
    *
    * @return It Works
    */
   @GET
   @Produces(mediaType)
   public String test() {
      return "It Works UserResource!";
   }
}
