package it.polimi.imageserver.resource;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import com.sun.jersey.api.ConflictException;

import it.polimi.imageserver.model.ImageContainer;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.model.UserContainer;
import it.polimi.imageserver.storage.UserStorage;

@Path("/users")
public class UserContainerResource {

	@Context UriInfo uriInfo;

	@GET
	@Produces("application/json")
	public Response getUserContainer() {
		UserContainer uc = UserStorage.US.getUserContainer();
		return Response.ok(uc).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response postNewUser(@FormParam("name") String name) {	
		if(name==null)
			Response.status(Status.BAD_REQUEST).build();
		
		String userId = Integer.toString(UserStorage.US.getUserContainer().getUsers().size()+1);
		
		if(UserStorage.US.existUser(userId))
			throw new ConflictException("409 An user with the same id already exists.");
		
		String uriString = uriInfo.getAbsolutePath().toString();
		uriString = uriString + userId;
		
		ImageContainer imageContainer = new ImageContainer();
		User user = new User(userId, name, imageContainer, uriString);	
		UserStorage.US.addUser(user);

		URI uri = URI.create(uriString);
		return Response.created(uri).build();
	}
	
	@Path("{userId}")
	public UserResource getUserResource(@PathParam(value = "userId") String uniqueId) {
		return new UserResource(uniqueId,uriInfo);
	}
	
}
