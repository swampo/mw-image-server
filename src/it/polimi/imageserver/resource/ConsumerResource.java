package it.polimi.imageserver.resource;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.validator.routines.EmailValidator;
import org.glassfish.jersey.server.mvc.Viewable;
import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider;
import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider.Consumer;
import org.glassfish.jersey.server.oauth1.OAuth1Consumer;
import org.glassfish.jersey.server.oauth1.OAuth1Provider;

import it.polimi.imageserver.model.ConsumerBean;
import it.polimi.imageserver.model.ConsumerForm;


/**
 * Resource for consumer registration
 *
 */
@Path("/consumer")
public class ConsumerResource {

   @Context
   private OAuth1Provider provider;

   /**
    * Registration page
    *
    */
   @Path("/register")
   @GET
   @Produces(MediaType.TEXT_HTML)
   public Response registration() {
      return Response.ok(new Viewable("/consumer_registration"))
            .build();
   }

   private Response showRegistrationErrorForm() {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("error", "The consumer data are not valid");
      return Response.status(Status.BAD_REQUEST)
            .entity(new Viewable("/consumer_registration", model))
            .build();
   }

   /**
    * Do registration from webpage
    *
    */
   @Path("/register")
   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.TEXT_HTML)
   @Valid
   public Response doRegistrationFromPage(@BeanParam ConsumerForm form) {
      ConsumerBean consumer = registerConsumer(form);
      if (consumer != null) {
         Map<String, Object> map = new HashMap<String, Object>();
         map.put("consumerName", consumer.getName());
         map.put("consumerKey", consumer.getKey());
         map.put("consumerSecret", consumer.getSecret());
         return Response.ok(new Viewable("/consumer_success", map))
               .build();
      }
      return showRegistrationErrorForm();
   }

   /**
    * Do registration api
    *
    */
   @Path("/api-register")
   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_JSON)
   @Valid
   public Response doRegistration(@BeanParam ConsumerForm form) {
      ConsumerBean cb = registerConsumer(form);
      if (cb != null) {
         return Response.ok()
               .entity(cb)
               .build();
      }
      return Response.status(Status.BAD_REQUEST)
            .entity("{error:400, message:'Form data not valid'")
            .build();
   }

   /**
    * Register a consumer and return name, key and secret or null if data are not valid.
    *
    * @param form
    * required data that will be checked
    * @return ConsumerBean containing name, key, secret
    */
   private ConsumerBean registerConsumer(ConsumerForm form) {
      if (isValidForm(form)) {
         String consumerName = form.getEmail();
         final DefaultOAuth1Provider defProvider = (DefaultOAuth1Provider) provider;
         // OPTIONAL: check duplicate
         // Retrieve consumers already registered with this email
         // Set<Consumer> existingConsumer = defProvider.getConsumers(consumerName);
         defProvider.registerConsumer(consumerName, new MultivaluedHashMap<String, String>());
         Set<Consumer> consumers = defProvider.getConsumers(consumerName);
         OAuth1Consumer consumer = consumers.iterator()
               .next();
         String consumerKey = consumer.getKey();
         String consumerSecret = consumer.getSecret();
         return new ConsumerBean(consumerName, consumerKey, consumerSecret);
      }
      return null;
   }

   public boolean isValidForm(ConsumerForm form) {
      if (form.getEmail() == null) {
         return false;
      }
      if (form.getEmail()
            .equals("")) {
         return false;
      }
      EmailValidator ev = EmailValidator.getInstance();
      if (!ev.isValid(form.getEmail())) {
         return false;
      }
      if (form.getName() == null) {
         return false;
      }
      if (form.getName()
            .equals("")) {
         return false;
      }
      return true;
   }

   /**
    * Test resource online
    *
    * @return It Works
    */
   @GET
   @Produces(MediaType.APPLICATION_JSON)
   public Response test() {
      return Response.ok("It Works consumer resource")
            .build();
   }
}
