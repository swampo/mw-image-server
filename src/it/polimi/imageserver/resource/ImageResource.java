package it.polimi.imageserver.resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.FileUtils;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import it.polimi.imageserver.config.ImageServerConfig;
import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.exception.ImageNotCreatedException;
import it.polimi.imageserver.exception.ImageNotFoundException;
import it.polimi.imageserver.exception.ImageNotUpdatedException;
import it.polimi.imageserver.exception.UserNotFoundException;
import it.polimi.imageserver.filter.ImageResourceAuthorizationFilter;
import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.ImageResponse;
import it.polimi.imageserver.response.ImagesListResponse;

/**
 * Resource for users management
 *
 */

@Path("/images")
public class ImageResource {
   private final String mediaType = MediaType.APPLICATION_JSON;

   private static final String STORAGE_BASE_DIR = "images";

   private static final String DEFAULT_USER = "u_default";

   @Context
   UriInfo uriInfo;

   @Context
   ServletContext context;

   @Inject // For testing
   @EJB // For UserDAOdb (with Glassfish)
   UserDAO users;

   /**
    * PUBLIC
    * Upload a new image
    *
    * @param user
    * @return confirm user creation
    */
   @POST
   @Path("public/{user}/upload")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces(mediaType)
   public Response publicPostNewImage(@PathParam("user") String userId, @FormDataParam("file") InputStream fileStream,
         @FormDataParam("file") FormDataContentDisposition fileDisposition) {
      return doPostNewImage(true, userId, fileStream, fileDisposition);
   }

   /**
    * Upload a new image
    *
    * @param user
    * @return confirm user creation
    */
   @RolesAllowed("user")
   @POST
   @Path("{user}/upload")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces(mediaType)
   public Response postNewImage(@PathParam("user") String userId, @FormDataParam("file") InputStream fileStream,
         @FormDataParam("file") FormDataContentDisposition fileDisposition) {
      return doPostNewImage(false, userId, fileStream, fileDisposition);
   }

   private Response doPostNewImage(boolean test, String userId, InputStream fileStream, FormDataContentDisposition fileDisposition) {
      // Get the owner
      User owner = users.read(userId);
      String dir = owner.getId();
      // Store the stream received
      try {
         // Create new image
         String uid = UUID.randomUUID()
               .toString();
         String title = FileUtils.removeExtension(fileDisposition.getFileName());
         String extension = FileUtils.getExtension(fileDisposition.getFileName());
         Image image = new Image(uid, owner, title, extension);
         // Image uri with the id
         String method = "getImage";
         if (test) {
            method = "publicGetImage";
         }
         UriBuilder baseUriBuilder = uriInfo.getBaseUriBuilder()
               .path(ImageResource.class)
               .path(ImageResource.class, method)
               .resolveTemplate("user", owner.getId())
               .resolveTemplate("id", image.getId());
         URI imageUri = baseUriBuilder.build();
         image.setUri(imageUri.toString());
         // Store the image
         @SuppressWarnings("unused")
         String filePath = writeToFileServer(fileStream, image.getId(), dir);
         // Add to user
         owner.addImage(image);
         users.update(owner);
         ImageResponse imageResponse = new ImageResponse("Image uploaded", image);
         return Response.created(imageUri)
               .entity(imageResponse)
               .build();
      } catch (Exception ioe) {
         // ioe.printStackTrace();
         throw new ImageNotCreatedException();
      }
   }

   /**
    * PUBLIC
    * Update an existing image info.
    * If not exist return the error
    *
    * @param image
    * info
    * @return the image info updated or error
    */
   @PUT
   @Produces(mediaType)
   @Consumes(mediaType)
   @Path("public/{user}/update/{id}")
   public Response publicUpdate(@NotNull @PathParam("user") String userId, @NotNull @PathParam("id") String imageId, @NotNull Image imageNewValues) {
      return doUpdate(userId, imageId, imageNewValues);
   }

   /**
    * Update an existing image info.
    * If not exist return the error
    *
    * @param image
    * info
    * @return the image info updated or error
    */
   @RolesAllowed("user")
   @PUT
   @Produces(mediaType)
   @Consumes(mediaType)
   @Path("{user}/update/{id}")
   public Response update(@NotNull @PathParam("user") String userId, @NotNull @PathParam("id") String imageId, @NotNull Image imageNewValues) {
      return doUpdate(userId, imageId, imageNewValues);
   }

   private Response doUpdate(String userId, String imageId, Image imageNewValues) {
      // Search the user
      User owner = users.read(userId);
      if (owner == null) {
         throw new UserNotFoundException();
      }
      // Check user own the image
      if (!owner.hasImage(imageId)) {
         throw new ImageNotFoundException(imageId);
      }
      Image image = owner.getImage(imageId);
      // Backup the image in case of errors
      Image bckImage = new Image(image);
      // Update image and store it only at the end
      // Overwrite not editable fields
      imageNewValues.setId(image.getId());
      imageNewValues.setUri(image.getUri());
      imageNewValues.setCreated(image.getCreated());
      // Update the image
      image.updateValues(imageNewValues);
      // Set last update
      image.setLastUpdate(new Timestamp(new Date().getTime()));
      try {
         users.update(owner);
         ImageResponse imageResponse = new ImageResponse("Image updated", image);
         return Response.ok()
               .entity(imageResponse)
               .build();
      } catch (Exception e) {
         // e.printStackTrace();
         // Reset the image to old values
         image.updateValues(bckImage);
         throw new ImageNotUpdatedException(image);
      }
   }

   /**
    * PUBLIC
    * Get an image
    *
    * @param id
    * of the image
    * @return the image
    */
   @GET
   @Path("public/{user}/image/{id}")
   @Produces({"image/jpg", "image/png", "image/gif"})
   public Response publicGetImage(@NotNull @PathParam("user") String userId, @NotNull @PathParam("id") String imageId) {
      return doGetImage(userId, imageId);
   }

   /**
    * Get an image
    *
    * @param id
    * of the image
    * @return the image
    */
   @RolesAllowed("user")
   @GET
   @Path("{user}/image/{id}")
   @Produces({"image/jpg", "image/png", "image/gif"})
   public Response getImage(@NotNull @PathParam("user") String userId, @NotNull @PathParam("id") String imageId) {
      return doGetImage(userId, imageId);
   }

   private Response doGetImage(String userId, String imageId) {
      // Search the user
      User owner = users.read(userId);
      if (owner == null) {
         throw new UserNotFoundException();
      }
      // Check user own the image
      if (!owner.hasImage(imageId)) {
         throw new ImageNotFoundException(imageId);
      }
      Image image = owner.getImage(imageId);
      // Check image exist on storage
      String dir = users.read(DEFAULT_USER)
            .getId();
      if (!imageExist(dir, imageId)) {
         throw new ImageNotFoundException(imageId);
      }
      // File path
      String filePath = Paths.get(getStorageBasePath(), dir, imageId)
            .toString();
      File file = new File(filePath);
      ResponseBuilder responseBuilder = Response.ok((Object) file);
      ContentDisposition contentDisposition = ContentDisposition.type("attachment")
            .fileName(image.getTitle() + "." + image.getExtension())
            .creationDate(image.getCreated())
            .modificationDate(image.getLastUpdate())
            .readDate(new Date())
            .size(file.length())
            .build();
      responseBuilder.header("Content-Disposition", contentDisposition);
      return responseBuilder.build();
   }

   /**
    * PUBLIC
    * Get user's images list
    *
    * @param id
    * of the image
    * @return the image
    */
   @GET
   @Path("public/{user}/list")
   @Produces(mediaType)
   public Response publicGetImagesList(@NotNull @PathParam("user") String userId) {
      return doGetImagesList(userId);
   }

   /**
    * Get user's images list
    *
    * @param id
    * of the image
    * @return the image
    */
   @RolesAllowed("user")
   @GET
   @Path("{user}/list")
   @Produces(mediaType)
   public Response getImagesList(@NotNull @PathParam("user") String userId) {
      return doGetImagesList(userId);
   }

   private Response doGetImagesList(String userId) {
      User user = users.read(userId);
      if (user == null) {
         throw new UserNotFoundException();
      }
      //Set absolute URI
      if(!user.getUri().startsWith("http")){
         URI uri = uriInfo.getBaseUriBuilder()
               .path(user.getUri())
               .build();
         user.setUri(uri.toString());
      }
      ImagesListResponse imagesListResponse = new ImagesListResponse("Images found", user, new ArrayList<Image>(user.getImages()));
      return Response.ok()
            .entity(imagesListResponse)
            .build();
   }

   /**
    * Write input stream to file server
    *
    * @param inputStream
    * @param fileName
    * The name of the file
    * @param directory
    * The directory of the user
    * @throws IOException
    */
   private String writeToFileServer(InputStream inputStream, String fileName, String directory) throws IOException {
      if (inputStream == null) {
         return null;
      }
      if (fileName == null) {
         return null;
      }
      if (directory == null) {
         return null;
      }

      OutputStream outputStream = null;
      // Directory and file path
      String directoryPath = Paths.get(getStorageBasePath(), directory)
            .toString();
      String filePath = Paths.get(directoryPath, fileName)
            .toString();

      // If not exists create directory
      File dir = new File(directoryPath);
      if (!dir.mkdirs()) {
         // TODO throw exception;
      }

      // Write the stream
      try {
         File file = new File(filePath);
         outputStream = new FileOutputStream(file);
         int read = 0;
         byte[] bytes = new byte[1024]; // 16384
         while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
         }
         outputStream.flush();
      } catch (IOException ioe) {
         // ioe.printStackTrace();
      } finally {
         // release resource, if any
         outputStream.close();
      }
      return filePath;
   }

   /**
    * @return
    */
   private String getStorageBasePath() {
      if (ImageServerConfig.DEBUG_MODE) {
         // Testing
         return Paths.get(System.getProperty("user.dir"), "test", "upload", STORAGE_BASE_DIR)
               .toString();
      } else {
         // Deploy
         return Paths.get(context.getRealPath("/"), STORAGE_BASE_DIR)
               .toString();
      }
   }

   /*
    * Check if the image exist
    */
   private boolean imageExist(String dir, String id) {
      // File path
      String filePath = Paths.get(getStorageBasePath(), dir, id)
            .toString();
      File file = new File(filePath);
      if (!file.exists() || file.isDirectory()) {
         return false;
      }
      return true;
   }

   /**
    * Test resource online
    *
    * @return It Works
    */
   @GET
   @RolesAllowed("user")
   @Produces(mediaType)
   public String testOnline() {
      return "It Works ImageResource!";
   }
}
