/**
 *
 */
package it.polimi.imageserver.filter;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;

import it.polimi.imageserver.resource.ImageResource;

/**
 * This dynamic binding provider registers ImageResourceAuthorizationFilter
 * only for ImageResource methods that are not public. It will be executed during
 * application initialization phase.
 */
public class ImageResourceDynamicFeature implements DynamicFeature {
   /*
    * (non-Javadoc)
    *
    * @see javax.ws.rs.container.DynamicFeature#configure(javax.ws.rs.container.ResourceInfo, javax.ws.rs.core.FeatureContext)
    */
   @Override
   public void configure(ResourceInfo resourceInfo, FeatureContext context) {
      if (ImageResource.class.equals(resourceInfo.getResourceClass()) && !(resourceInfo.getResourceMethod()
            .getName()
            .contains("testOnline") || resourceInfo.getResourceMethod()
            .getName()
            .contains("public"))) {
         context.register(ImageResourceAuthorizationFilter.class);
      }
   }
}
