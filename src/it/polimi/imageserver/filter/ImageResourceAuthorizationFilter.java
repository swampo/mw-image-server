package it.polimi.imageserver.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import it.polimi.imageserver.model.User;

/**
 * Authorization filter for ImageResource
 *
 */
/*
 * NB: RolesAllowedDynamicFeature set a filter with priority 2000.
 * To execute this filter later, set priority at least 2001
 */
@Priority(2001)
public class ImageResourceAuthorizationFilter implements ContainerRequestFilter {
   private static final String ROLE = "user";

   @Override
   public void filter(ContainerRequestContext requestContext) throws IOException {
      final SecurityContext securityContext = requestContext.getSecurityContext();
      final UriInfo uriInfo = requestContext.getUriInfo();

      if (securityContext == null || !securityContext.isUserInRole(ROLE)) {
         requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
               .entity("User cannot access the resource.")
               .build());
      }

      if (uriInfo == null) {
         requestContext.abortWith(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity("Error loading uri info")
               .build());
      }

      User user = (User) securityContext.getUserPrincipal();
      String userId = uriInfo.getPathParameters()
            .getFirst("user");

      if (userId == null) {
         requestContext.abortWith(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity("Error loading user id from uri")
               .build());
      }

      if (!user.getId()
            .equals(userId)) {
         requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
               .entity("User cannot access the resource. Required userId different from id associated to the given user")
               .build());
      }
   }
}
