package it.polimi.imageserver.database;
//import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public final class DBStorage {

    public static ResultSet query(String query) throws NamingException {
        Context context = null;
        DataSource datasource = null;
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            // Get the context and create a connection
            context = new InitialContext();
            datasource = (DataSource) context.lookup("java:/comp/env/jdbc/mw_imageserver");
            connect = datasource.getConnection();

            // Create the statement to be used to get the results.
            statement = connect.createStatement();

            // Execute the query and get the result set.
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) { e.printStackTrace();
        } finally {
            // Close the connection and release the resources used.
            try { statement.close(); } catch (SQLException e) { e.printStackTrace(); }
            try { connect.close(); } catch (SQLException e) { e.printStackTrace(); }
        }

        return resultSet;
    }
}
