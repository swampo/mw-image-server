/**
 *
 */
package it.polimi.imageserver.config;

import java.security.Principal;
import java.util.logging.Logger;

import javax.ws.rs.core.MultivaluedHashMap;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;
import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider;
import org.glassfish.jersey.server.oauth1.OAuth1ServerFeature;
import org.glassfish.jersey.server.oauth1.OAuth1ServerProperties;

import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.dao.UserDAOdb;
import it.polimi.imageserver.filter.ImageResourceDynamicFeature;
import jersey.repackaged.com.google.common.collect.Sets;

/**
 * ImageServerConfig set the application configurations.
 * Register the resources , providers, features and filters
 *
 */
public class ImageServerConfig extends ResourceConfig {
   public static final boolean DEBUG_MODE = true;

   private static final String REQUEST_TOKEN_URI = "request-token";
   private static final String ACCESS_TOKEN_URI = "access-token";
   // Test Consumer
   private static final String SECRET_CONSUMER_KEY = "secret-consumer-key";
   private static final String CONSUMER_KEY = "my-consumer-key";
   private static final String CONSUMER_NAME = "my-consumer";
   private static final String TEST_TOKEN = "test-token";
   private static final String TEST_SECRET = "test-secret";

   public ImageServerConfig() {
      // Instance of logging filter with the feature "print entity" set to true
      registerInstances(new LoggingFilter(Logger.getLogger("ImageServerLogger"), true));

      // MOXyJson Provider
      register(MOXyJsonContextResolver.class);

      // MultiPart
      register(MultiPartFeature.class);

      // DAOs injection
      // UserDAO users = new UserDAOmap(); //for testing
      register(new AbstractBinder() {
         @Override
         protected void configure() {
            // bind(users).to(UserDAO.class); //for testing
            bind(UserDAOdb.class).to(UserDAO.class)
                  .in(RequestScoped.class);
         }
      });

      // Add support to jsp
      register(JspMvcFeature.class);
      property(JspMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/jsp");

      configureOAuth();

      // Add resources
      packages("it.polimi.imageserver.resource");
   }

   /**
    * Configure oAuth1 feature
    */
   private void configureOAuth() {
      /*
       * oAuth 1 Configuration
       */
      // Default OAuth1Provider
      final DefaultOAuth1Provider oAuthProvider = new DefaultOAuth1Provider();

      // Test Consumer
      oAuthProvider.registerConsumer(CONSUMER_NAME, CONSUMER_KEY, SECRET_CONSUMER_KEY, new MultivaluedHashMap<String, String>());
      // OAuth1ServerFeature add to the servlet the oAuth1 support
      // A OAuth1Provider is required
      final OAuth1ServerFeature oAuth1ServerFeature = new OAuth1ServerFeature(oAuthProvider, REQUEST_TOKEN_URI, ACCESS_TOKEN_URI);

      register(oAuth1ServerFeature);
      /*
       * OAuth1ServerFilter will be automatically registered
       *
       * OAuth request filter that filters all requests indicating in the
       * Authorization header they use OAuth. Checks if the incoming requests
       * are properly authenticated and populates the security context with
       * the corresponding user principal and roles.
       *
       * Protected resources must be annotated with @RolesAllowed
       */
      register(RolesAllowedDynamicFeature.class);
      // ImageResourceAuthorization filter
      register(ImageResourceDynamicFeature.class);

      // OAuth1 properties
      property(OAuth1ServerProperties.TIMESTAMP_UNIT, "SECONDS");
      property(OAuth1ServerProperties.MAX_NONCE_CACHE_SIZE, 20);
      property(OAuth1ServerProperties.IGNORE_PATH_PATTERN, "authorize/*");
   }
}
