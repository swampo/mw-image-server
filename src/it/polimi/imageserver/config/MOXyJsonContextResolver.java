package it.polimi.imageserver.config;

import javax.ws.rs.ext.*;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;

/**
 * Configuration for MOXy (the default JSON-binding provider)
 * Return an instance of MOXyJsonConfig with the configurations
 *
 */
@Provider
public class MOXyJsonContextResolver implements ContextResolver<MoxyJsonConfig> {

	private final MoxyJsonConfig config;

	public MOXyJsonContextResolver() {
		config = new MoxyJsonConfig().setIncludeRoot(true);
	}

	@Override
	public MoxyJsonConfig getContext(Class<?> objectType) {
		return config;
	}

}