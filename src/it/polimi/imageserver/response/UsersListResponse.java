/**
 *
 */
package it.polimi.imageserver.response;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.polimi.imageserver.model.User;

/**
 * User list response returned by UserResource
 * This response contains:
 * status: the status of the request (SUCCESS or FAIL)
 * errorCode: code of the error occured (0 in case of success)
 * message: message about the correct execution of the request
 * users: the list of stored users about
 *
 * @author Andrea Salmoiraghi
 *
 */
@XmlRootElement
public class UsersListResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@XmlElement
	private int errorCode;
	@XmlElement
	private String status;
	@XmlElement
	private String message;
	@XmlElement
	private List<User> users;


	public UsersListResponse() {
	}

	/**
	 * Constructor for success request
	 * @param message
	 * @param users
	 */
	public UsersListResponse(String message, List<User> users) {
		this.errorCode = 0;
		this.status = "SUCCESS";
		this.message = message;
		this.users = users;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
