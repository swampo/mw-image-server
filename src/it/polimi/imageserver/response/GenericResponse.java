package it.polimi.imageserver.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * This response contains:
 * status: the status of the request (SUCCESS or FAIL)
 * errorCode: code of the error occured (0 in case of success)
 * message: message about the correct execution of the request
 *
 */
@XmlRootElement
public class GenericResponse implements Serializable {
   private static final long serialVersionUID = 1L;

   @XmlElement
   protected int errorCode = 50000;
   @XmlElement
   protected String status = "FAIL";
   @XmlElement
   protected String message = "Generic server error";

   /*
    * The empty constructor is mandatory because it needs to MessageBodyWriter
    * to create Json/Xml automatically
    */
   public GenericResponse() {
   }

   /**
    *
    * Constructor for error
    *
    * @param errorCode
    * @param status
    * @param message
    */
   public GenericResponse(int errorCode, String status, String message) {
      this.errorCode = errorCode;
      this.status = status;
      this.message = message;
   }

   public int getErrorCode() {
      return errorCode;
   }

   public void setErrorCode(int errorCode) {
      this.errorCode = errorCode;
   }

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

}
