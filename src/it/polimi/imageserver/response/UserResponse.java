package it.polimi.imageserver.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.polimi.imageserver.model.User;

/**
 * User response returned by UserResource
 * This response contains:
 * status: the status of the request (SUCCESS or FAIL)
 * errorCode: code of the error occured (0 in case of success)
 * message: message about the correct execution of the request
 * user: the user about the request
 *
 */
@XmlRootElement
public class UserResponse extends GenericResponse implements Serializable {
   private static final long serialVersionUID = 1L;

   @XmlElement
   private User user;

   /*
    * The empty constructor is mandatory because it needs to MessageBodyWriter
    * to create Json/Xml automatically
    */
   public UserResponse() {}

   /**
    *
    * Constructor for error without user
    *
    * @param errorCode
    * @param status
    * @param message
    */
   public UserResponse(int errorCode, String status, String message) {
      super(errorCode, status, message);
   }

   /**
    * Constructor for error with user
    *
    * @param errorCode
    * @param status
    * @param message
    * @param user
    */
   public UserResponse(int errorCode, String status, String message, User user) {
      super(errorCode, status, message);
      this.user = user;
   }

   /**
    * Constructor for success request
    *
    * @param message
    * @param user
    */
   public UserResponse(String message, User user) {
      super(0, "SUCCESS", message);
      this.user = user;
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }

}
