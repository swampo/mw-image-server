package it.polimi.imageserver.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.polimi.imageserver.model.Image;

/**
 * Image response returned by ImageResource
 * This response contains:
 * status: the status of the request (SUCCESS or FAIL)
 * errorCode: code of the error occured (0 in case of success)
 * message: message about the correct execution of the request
 *
 */
@XmlRootElement
public class ImageResponse extends GenericResponse {
   private static final long serialVersionUID = 1L;

   @XmlElement
   private Image image;

   /*
    * The empty constructor is mandatory because it needs to MessageBodyWriter
    * to create Json/Xml automatically
    */
   public ImageResponse() {}

   /**
    *
    * Constructor for error
    *
    * @param errorCode
    * @param status
    * @param message
    */
   public ImageResponse(int errorCode, String status, String message) {
      super(errorCode, status, message);
   }

   /**
    * Constructor for error with image
    *
    * @param errorCode
    * @param status
    * @param message
    * @param image
    */
   public ImageResponse(int errorCode, String status, String message, Image image) {
      super(errorCode, status, message);
      this.image = image;
   }

   /**
    * Constructor for success request
    *
    * @param message
    * @param user
    */
   public ImageResponse(String message, Image image) {
      super(0, "SUCCESS", message);
      this.image = image;
   }

   /**
    * Get the image
    * @return the image
    */
   public Image getImage() {
      return image;
   }

   /**
    * Set the image
    * @param image
    */
   public void setImage(Image image) {
      this.image = image;
   }
}
