/**
 *
 */
package it.polimi.imageserver.response;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.User;

/**
 * Images list response returned by ImageResource
 * This response contains:
 * status: the status of the request (SUCCESS or FAIL)
 * errorCode: code of the error occured (0 in case of success)
 * message: message about the correct execution of the request
 * user: the owner
 * images: the list of images
 *
 */
@XmlRootElement
public class ImagesListResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@XmlElement
	private int errorCode;
	@XmlElement
	private String status;
	@XmlElement
	private String message;
	@XmlElement
	private User owner;
	@XmlElement
	private List<Image> images;


	public ImagesListResponse() {
	}

	/**
	 * Constructor for success request
	 * @param message
	 * @param user owner
	 * @param images
	 */
	public ImagesListResponse(String message, User owner, List<Image> images) {
		this.errorCode = 0;
		this.status = "SUCCESS";
		this.message = message;
		this.owner = owner;
		this.images = images;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
}
