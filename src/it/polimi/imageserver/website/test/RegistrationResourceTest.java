package it.polimi.imageserver.website.test;

import static org.junit.Assert.*;

import java.net.URI;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import it.polimi.imageserver.website.resource.UserRegistrationResource;

//#############NOT WORK: TO CHECK

/**
 * Test for UserSiteResource
 *
 */
public class RegistrationResourceTest extends JerseyTest {

   private static final String acceptedMediaType = MediaType.TEXT_HTML;
   private static final String resourcePath = UriBuilder.fromResource(UserRegistrationResource.class)
         .toString();

   /*
    * Application configuration for testing
    *
    * (non-Javadoc)
    *
    * @see org.glassfish.jersey.test.JerseyTest#configure()
    */
   @Override
   protected Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      return new ResourceConfig(UserRegistrationResource.class).register(JspMvcFeature.class)
            .property(JspMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/jsp");
   }

   @Override
   protected void configureClient(ClientConfig clientConfig) {
      // Register custom MOXyJsonProvider
      // clientConfig.register(MOXyJsonContextResolver.class);
   }

   /**
    * Check if the resource is available
    */
   @Test
   public void testOnline() {
      Response response = target(resourcePath).path("online")
            .request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
   }

}
