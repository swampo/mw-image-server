/**
 *
 */
package it.polimi.imageserver.website.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.FormParam;

import org.junit.validator.ValidateWith;


/**
 * UserForm bean
 */
public class UserForm {
   @NotNull
   @ValidateWith(EmailChecker.class)
   @FormParam("email")
   protected String email;
   @FormParam("password")
   protected String password;
   @NotNull
   @Size(min=2, max=50)
   @FormParam("name")
   protected String name;

   public UserForm(){
   }

   public UserForm(String name, String email) {
      this.name = name;
      this.email = email;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }
}
