/**
 *
 */
package it.polimi.imageserver.website.model;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.validator.routines.EmailValidator;
import org.junit.runners.model.FrameworkField;
import org.junit.validator.AnnotationValidator;

/**
 * Check valid mail
 *
 */
public class EmailChecker extends AnnotationValidator {

   @Override
   public List<Exception> validateAnnotatedField(FrameworkField field) {
      List<Exception> errors = new ArrayList<Exception>();
      EmailValidator ev = EmailValidator.getInstance();
      if(!ev.isValid(field.toString())){
         String message = "The email is not valid";
         errors.add(new Exception(message));
      }
      return errors;
   }

}
