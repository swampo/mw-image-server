package it.polimi.imageserver.website.resource;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.validator.routines.EmailValidator;
import org.glassfish.jersey.server.mvc.Viewable;

import it.polimi.imageserver.exception.UserNotValidException;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.resource.UserResource;
import it.polimi.imageserver.website.model.UserForm;

/**
 * Resource for user webpage
 *
 */
@Path("/registration")
public class UserRegistrationResource {

   private final String mediaType = MediaType.TEXT_HTML;

   /**
    * Do registration
    *
    */
   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(mediaType)
   @Valid
   public Response doRegistration(@BeanParam UserForm form) {
      // TO DO: controlli
      if (isValidForm(form)) {
         User u = new User(form.getName(), form.getEmail(), form.getPassword());
         UserResource ur = new UserResource();
         try {
            Response resp = ur.create(u);
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("location", resp.getHeaderString("Location"));
            return Response.ok(new Viewable("/registration_success", model))
                  .build();
         } catch (Exception e) {
            return showRegistrationErrorCreate();
         }
      }
      return showRegistrationErrorForm();
   }

   public boolean isValidForm(UserForm user) {
      if (user.getEmail() == null) {
         return false;
      }
      if (user.getPassword() == null) {
         return false;
      }
      if (user.getEmail()
            .equals("")) {
         return false;
      }
      EmailValidator ev = EmailValidator.getInstance();
      if (!ev.isValid(user.getEmail())) {
         return false;
      }
      return true;
   }

   /**
    * Registration page
    *
    */
   @GET
   @Produces(mediaType)
   public Response registration() {
      return showRegistration(null);
   }

   private Response showRegistration(Map<String, Object> model) {
      return Response.ok(new Viewable("/registration", model))
            .build();
   }

   private Response showRegistrationErrorForm() {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("error", "The user data are not valid");
      return showRegistration(model);
   }

   private Response showRegistrationErrorCreate() {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("error", "Errors occured during user creation. Please try again");
      return showRegistration(model);
   }

   /**
    * Test resource online
    *
    * @return It Works
    */
   @Path("online")
   @GET
   @Produces(mediaType)
   public Response test() {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("resource", this.getClass()
            .getName());
      return Response.ok(new Viewable("/online", model))
            .build();
   }
}
