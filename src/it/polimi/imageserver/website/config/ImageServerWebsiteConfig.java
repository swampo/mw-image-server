package it.polimi.imageserver.website.config;

import java.util.logging.Logger;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;


/**
 * Set the application configurations.
 * Register the resources , providers, features and filters
 *
 */
@ApplicationPath("/site")
public class ImageServerWebsiteConfig extends ResourceConfig {

	public ImageServerWebsiteConfig() {
		//Instance of logging filter with the feature "print entity" set to true
		registerInstances(new LoggingFilter(Logger.getLogger("ImageServerWebsiteLogger"),true));

		//Add support to jsp
        register(JspMvcFeature.class);
        property(JspMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/jsp");

		//Add resources
		packages("it.polimi.imageserver.website.resource");
	}
}
