package it.polimi.imageserver.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.security.Principal;
import java.text.ParseException;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.oauth1.AccessToken;
import org.glassfish.jersey.client.oauth1.ConsumerCredentials;
import org.glassfish.jersey.client.oauth1.OAuth1ClientSupport;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider;
import org.glassfish.jersey.server.oauth1.OAuth1ServerFeature;
import org.glassfish.jersey.server.oauth1.OAuth1ServerProperties;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.dao.UserDAOmap;
import it.polimi.imageserver.filter.ImageResourceDynamicFeature;
import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.resource.ImageResource;
import it.polimi.imageserver.response.ImageResponse;
import it.polimi.imageserver.response.ImagesListResponse;
import it.polimi.imageserver.response.UserResponse;
import jersey.repackaged.com.google.common.collect.Sets;

/**
 * Test ImageResource
 *
 */
public class ImageResourceTestOAuth extends JerseyTest {

   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;
   private String resourcePath = UriBuilder.fromResource(ImageResource.class)
         .toString();

   private static final String pathGet = "u_default/image";
   private static final String pathGetList = "u_default/list";
   private static final String pathUpload = "u_default/upload";
   private static final String pathUpdate = "u_default/update";

   private static final String REQUEST_TOKEN_ENDPOINT = "request-token";
   private static final String ACCESS_TOKEN_ENDPOINT = "access-token";
   // Test Consumer
   private static final String SECRET_CONSUMER_KEY = "secret-consumer-key";
   private static final String CONSUMER_KEY = "my-consumer-key";
   private static final String CONSUMER_NAME = "my-consumer";
   private static final String TEST_TOKEN = "test-token";
   private static final String TEST_SECRET = "test-secret";
   // Default user
   private static final String DEFAULT_USER_ID = "u_default";
   private User defaultUser;

   /*
    * Application configuration for testing
    *
    * (non-Javadoc)
    *
    * @see org.glassfish.jersey.test.JerseyTest#configure()
    */
   @Override
   protected Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      // DAOs injection
      UserDAO users = new UserDAOmap();
      defaultUser = users.read(DEFAULT_USER_ID);
      ResourceConfig resconf = new ResourceConfig(ImageResource.class);
      resconf.register(new AbstractBinder() {
         @Override
         protected void configure() {
            bind(users).to(UserDAO.class);
         }
      })
            .register(MOXyJsonContextResolver.class)
            .register(MultiPartFeature.class);
      configureOAuth(resconf);
      return resconf;
   }

   /**
    * Configure oAuth1 feature
    */
   private void configureOAuth(ResourceConfig resconf) {
      /*
       * oAuth 1 Configuration
       */
      // Default OAuth1Provider
      final DefaultOAuth1Provider oAuthProvider = new DefaultOAuth1Provider();
      // Test Consumer
      oAuthProvider.registerConsumer(CONSUMER_NAME, CONSUMER_KEY, SECRET_CONSUMER_KEY, new MultivaluedHashMap<String, String>());
      // Register access token for default user
      oAuthProvider.addAccessToken(TEST_TOKEN, TEST_SECRET, CONSUMER_KEY, "http://callback.url", defaultUser, Sets.newHashSet("admin", "user"),
            new MultivaluedHashMap<String, String>());
      // OAuth1ServerFeature add to the servlet the oAuth1 support
      // A OAuth1Provider is required
      final OAuth1ServerFeature oAuth1ServerFeature = new OAuth1ServerFeature(oAuthProvider, REQUEST_TOKEN_ENDPOINT, ACCESS_TOKEN_ENDPOINT);
      resconf.register(oAuth1ServerFeature);
      // OAuth1 properties
      resconf.property(OAuth1ServerProperties.TIMESTAMP_UNIT, "SECONDS");
      resconf.property(OAuth1ServerProperties.MAX_NONCE_CACHE_SIZE, 20);
      resconf.property(OAuth1ServerProperties.IGNORE_PATH_PATTERN, "authorize/*");
      // Roles Filtes
      resconf.register(RolesAllowedDynamicFeature.class);
      // ImageResourceAuthorization filter
      resconf.register(ImageResourceDynamicFeature.class);
   }

   @Override
   protected void configureClient(ClientConfig clientConfig) {
      // enable(TestProperties.LOG_TRAFFIC);
      // enable(TestProperties.DUMP_ENTITY);
      clientConfig.register(MOXyJsonContextResolver.class);
      clientConfig.register(MultiPartFeature.class);

      // OAuth feature
      final Feature filterFeature = OAuth1ClientSupport.builder(new ConsumerCredentials(CONSUMER_KEY, SECRET_CONSUMER_KEY))
            .feature()
            .accessToken(new AccessToken(TEST_TOKEN, TEST_SECRET))
            .build();
      // Add the feature to get authorized client
      clientConfig.register(filterFeature);
   }

   /**
    * Check if the resource is available
    */
   @Test
   public void userResourceOnline() {
      Response response = target(resourcePath).request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
   }

   /**
    * Test upload with user id in path different from user id of the accessToken
    */
   @Test
   public void differenUserIds() {
      Response response = target(resourcePath).path("other_user/list")
            .request(acceptedMediaType)
            .get();
      assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
   }

   /**
    * Test to upload an image
    */
   @Test
   public void uploadImageTest() {
      String absolutePath = System.getProperty("user.dir");
      File fileToUpload = new File(absolutePath + "/test/batman.png");
      FormDataMultiPart multiPart = new FormDataMultiPart();
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      Response response = target(resourcePath).path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      assertNotNull(response.getHeaderString("Location"));
      assertEquals(true, response.hasEntity());
      ImageResponse imageResponse = response.readEntity(ImageResponse.class);
      assertNotNull(imageResponse);
      assertEquals(0, imageResponse.getErrorCode());
      Image image = imageResponse.getImage();
      assertNotNull(image);
      // assertEquals(UriBuilder.fromUri(baseAppUri)
      // .path(image.getUri())
      // .toString(), response.getHeaderString("Location"));
      assertEquals(image.getUri(), response.getHeaderString("Location"));
      List<Image> images = getImagesList();
      assertEquals(false, images.isEmpty());
      int size = images.size();
      // Second upload
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      response = target(resourcePath).path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      images = getImagesList();
      assertEquals(size + 1, images.size());

   }

   /**
    * Update the image info
    */
   @Test
   public void updateImage() {
      // Upload image
      String absolutePath = System.getProperty("user.dir");
      File fileToUpload = new File(absolutePath + "/test/batman.png");
      FormDataMultiPart multiPart = new FormDataMultiPart();
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      Response response = target(resourcePath).path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      assertEquals(true, response.hasEntity());
      ImageResponse imageResponse = response.readEntity(ImageResponse.class);
      assertNotNull(imageResponse);
      assertEquals(0, imageResponse.getErrorCode());
      Image image = imageResponse.getImage();
      assertNotNull(image);
      // Update info
      image.setTitle("New Title");
      response = target(resourcePath).path(pathUpdate)
            .path(image.getId())
            .request(acceptedMediaType)
            .put(Entity.entity(image, acceptedMediaType));
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      imageResponse = response.readEntity(ImageResponse.class);
      assertNotNull(imageResponse);
      assertEquals(0, imageResponse.getErrorCode());
      assertEquals("SUCCESS", imageResponse.getStatus());
      Image imageUpdated = imageResponse.getImage();
      assertNotNull(imageUpdated);
      assertEquals(imageUpdated, image);
      assertEquals(image, imageUpdated);
      assertEquals(image.getTitle(), imageUpdated.getTitle());
   }

   /*
    * Get the user's images list
    */
   private List<Image> getImagesList() {
      Response response;
      ImagesListResponse imagesListResponse;
      response = target(resourcePath).path(pathGetList)
            .request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      imagesListResponse = response.readEntity(ImagesListResponse.class);
      assertNotNull(imagesListResponse);
      assertEquals(0, imagesListResponse.getErrorCode());
      assertEquals("SUCCESS", imagesListResponse.getStatus());
      return imagesListResponse.getImages();
   }

   /**
    * Test to download an image
    */
   @Test
   public void getImageTest() {
      String image = "testDownload";
      String donwloadBasePath = System.getProperty("user.dir") + "/test/download/";
      Response response = target(resourcePath).path(pathGet)
            .path(image)
            .request("image/png")
            .get();
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      String scd = response.getHeaderString("Content-Disposition");
      assertNotNull(scd);
      String fileName = "default.png";
      try {
         ContentDisposition cd = new ContentDisposition(scd);
         fileName = cd.getFileName();
         assertNotNull(fileName);
      } catch (ParseException e) {
         System.out.println("Error parsing content disposition");
         return;
      }
      InputStream inputStream = response.readEntity(InputStream.class);
      // If not exists create directory
      String downloadFilePath = donwloadBasePath + fileName;
      File dir = new File(donwloadBasePath);
      if (!dir.mkdirs()) {
         // TODO throw exception;
      }
      OutputStream outputStream = null;
      try {
         File file = new File(downloadFilePath);
         outputStream = new FileOutputStream(file);
         int read = 0;
         byte[] bytes = new byte[1024]; // 16384
         while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
         }
         outputStream.flush();
      } catch (IOException ioe) {
         ioe.printStackTrace();
      } finally {
         // release resource, if any
         try {
            outputStream.close();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }

   /**
    * Test image not found
    */
   @Test
   public void getImageNotFoundTest() {
      String image = "xyz.png";
      Response response = target(resourcePath).path(pathGet)
            .path(image)
            .request("image/png")
            .get();
      assertEquals(404, response.getStatus());
      assertEquals(true, response.hasEntity());
      ImageResponse errorResponse = response.readEntity(ImageResponse.class);
      assertNotNull(errorResponse);
      assertEquals(4054, errorResponse.getErrorCode());
      assertEquals("FAIL", errorResponse.getStatus());
   }
}
