package it.polimi.imageserver.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.response.ImageResponse;
import it.polimi.imageserver.response.ImagesListResponse;

/*
 * RUN AS JAVA APPLICATION
 */
public class ImageResourceClientTestPublic {

   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;

   // User for testing: u_default
   private static final String pathGet = "public/u_default/image";
   private static final String pathGetList = "public/u_default/list";
   private static final String pathUpload = "public/u_default/upload";
   private static final String pathUpdate = "public/u_default/update";

   public static void main(String[] args) {
      Response r;

      // Client configuration
      ClientConfig clientConfig = new ClientConfig();
      // Register custom MOXyJsonProvider
      clientConfig.register(MOXyJsonContextResolver.class);
      // Register Logging filter
      clientConfig.register(new LoggingFilter(Logger.getLogger("ImageServerLogger"), true));
      // Multipart
      clientConfig.register(MultiPartFeature.class);

      // Obtain new client
      Client client = ClientBuilder.newClient(clientConfig);

      // ImageServer target
      WebTarget webTarget = client.target("http://localhost:8080/mw_imageserver/rest")
            .path("images");

      // Test upload
      String absolutePath = System.getProperty("user.dir");
      File fileToUpload = new File(absolutePath + "/test/batman.png");
      FormDataMultiPart multiPart = new FormDataMultiPart();
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      r = webTarget.path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      if (r.getStatus() != 201) {
         System.out.println("Error: Image not uploaded");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      ImageResponse iur = r.readEntity(ImageResponse.class);
      System.out.println("Image response correctly received");
      if (iur.getErrorCode() != 0) {
         System.out.println("Error: Image not uploaded");
         return;
      }
      Image image = iur.getImage();
      System.out.println("[IMAGE] " + image.toString());

      // Test Update
      System.out.println("Old title: " + image.getTitle());
      image.setTitle("il_cavaliere_oscuro");
      System.out.println("Set new title: " + image.getTitle());
      r = webTarget.path(pathUpdate)
            .path(image.getId())
            .request(acceptedMediaType)
            .put(Entity.entity(image, acceptedMediaType));
      if (r.getStatus() != 200) {
         System.out.println("Error: Image not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      ImageResponse iupr = r.readEntity(ImageResponse.class);
      System.out.println("User response correctly received");
      if (iupr.getErrorCode() != 0) {
         System.out.println("Error: Image not found");
         return;
      }
      image = iupr.getImage();
      System.out.println("[IMAGE] " + image.toString());

      // Test get images list
      r = webTarget.path(pathGetList)
            .request(acceptedMediaType)
            .get();
      if (r.getStatus() != 200) {
         System.out.println("Error: Image list not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      ImagesListResponse ilr = r.readEntity(ImagesListResponse.class);
      System.out.println("User response correctly received");
      if (ilr.getErrorCode() != 0) {
         System.out.println("Error: Images list not returned");
         return;
      }
      List<Image> images = ilr.getImages();
      for (Image i : images) {
         System.out.println(i);
      }

      //Test download
      //NOTE: when starts glassfish erase the directory. Reupload testDownload
      String donwloadBasePath = System.getProperty("user.dir") + "/test/download_deploy/";
      r = webTarget.path(pathGet)
            .path("testDownload")
            .request("image/png")
            .get();
      if (r.getStatus() != 200) {
         System.out.println("Error: Image not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      String scd = r.getHeaderString("Content-Disposition");
      String fileName = "default.png";
      if (scd != null) {
         try {
            ContentDisposition cd = new ContentDisposition(scd);
            fileName = cd.getFileName();
         } catch (ParseException e) {
            System.out.println("Error parsing content disposition");
            return;
         }
      }
      InputStream inputStream = r.readEntity(InputStream.class);
      // If not exists create directory
      String downloadFilePath = donwloadBasePath + fileName;
      File dir = new File(donwloadBasePath);
      if (!dir.mkdirs()) {
         // TODO throw exception;
      }
      OutputStream outputStream = null;
      try {
         File file = new File(downloadFilePath);
         outputStream = new FileOutputStream(file);
         int read = 0;
         byte[] bytes = new byte[1024]; // 16384
         while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
         }
         outputStream.flush();
         System.out.println("Image downloaded!");
      } catch (IOException ioe) {
         ioe.printStackTrace();
      } finally {
         // release resource, if any
         try {
            outputStream.close();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }

      client.close();
      System.out.println("TEST FINISHED WITHOUT ERRORS");
   }

}
