package it.polimi.imageserver.test;

import static org.junit.Assert.*;

import java.net.URI;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.dao.UserDAOmap;
import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.resource.UserResource;
import it.polimi.imageserver.response.UserResponse;

/**
 * Test for UserResource with Mockito
 *
 */
public class UserResourceMockitoTest extends JerseyTest {

   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;
   private static final String resourcePath = UriBuilder.fromResource(UserResource.class)
         .toString();
   private URI baseAppUri = getBaseUri();

   private static final String pathGet = "info";
   private static final String pathCreate = "create";
   private static final String pathList = "list";

   private static UserDAO userDAOMock = Mockito.mock(UserDAOmap.class);

   /*
    * Application configuration for testing
    *
    * (non-Javadoc)
    *
    * @see org.glassfish.jersey.test.JerseyTest#configure()
    */
   @Override
   protected Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      // DAOs mock injection
      return new ResourceConfig(UserResource.class).register(new AbstractBinder() {
         @Override
         protected void configure() {
            bind(userDAOMock).to(UserDAO.class);
         }
      })
            .register(MOXyJsonContextResolver.class);
   }

   @Override
   protected void configureClient(ClientConfig clientConfig) {
      // Register custom MOXyJsonProvider
      clientConfig.register(MOXyJsonContextResolver.class);
   }

   /**
    * Check if the resource is available
    */
   @Test
   public void userResourceOnline() {
      Response response = target(resourcePath).request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
   }

   /**
    * Create an user
    */
   @Test
   public void createUser() {
      User user = new User("TestCreate", "testcreate@test.it", "TestCreate");
      User userCreated = new User(user);
      userCreated.setId("uniqueId");
      userCreated.setUri("uniqueUri");
      try {
         // Reference equals only based on the name sent by the client
         Mockito.when(userDAOMock.create(Mockito.refEq(user, "id", "uri", "created ", "lastUpdate")))
               .thenReturn(userCreated);
      } catch (Exception e1) {
         // e1.printStackTrace();
      }
      Response response = target(resourcePath).path(pathCreate)
            .request(acceptedMediaType)
            .post(Entity.entity(user, acceptedMediaType));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      assertNotNull(response.getHeaderString("Location"));
      assertEquals(true, response.hasEntity());
      UserResponse userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(0, userResponse.getErrorCode());
      assertEquals("SUCCESS", userResponse.getStatus());
      User receivedUser = userResponse.getUser();
      assertNotNull(receivedUser);
      assertEquals(receivedUser.getUri(), response.getHeaderString("Location"));
   }

   /**
    * User not created exception
    */
   @Test
   public void userNotCreated() {
      User user = new User("TestUserNotCreated", "testusernotcreated@test.it", "TestUserNotCreated");
      try {
         // Reference equals only based on the name sent by the client
         Mockito.doThrow(new Exception())
               .when(userDAOMock)
               .create(Mockito.refEq(user, "id", "uri"));
      } catch (Exception e1) {
         // e1.printStackTrace();
      }
      Response response = target(resourcePath).path(pathCreate)
            .request(acceptedMediaType)
            .post(Entity.entity(user, acceptedMediaType));
      assertEquals(500, response.getStatus());
      assertEquals(true, response.hasEntity());
      UserResponse userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(4005, userResponse.getErrorCode());
      assertEquals("FAIL", userResponse.getStatus());
      User receivedUser = userResponse.getUser();
      assertNotNull(receivedUser);
   }

   /**
    * User not found
    */
   @Test
   public void userNotFound() {
      String uri = "uriNotCreated";
      User user = new User("TestUserNotFound", "testusernotfound@test.it", "TestUserNotFound");
      user.setId("uniqueId");
      user.setUri(uri);
      Mockito.when(userDAOMock.read(user.getUri()))
            .thenReturn(null);
      Response response = target(resourcePath).path(pathGet)
            .path(uri)
            .request(acceptedMediaType)
            .get();
      // 404 resource not found
      assertEquals(404, response.getStatus());
      assertEquals(true, response.hasEntity());
      UserResponse userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(4004, userResponse.getErrorCode());
      assertEquals("FAIL", userResponse.getStatus());
      User userReceived = userResponse.getUser();
      assertNull(userReceived);
   }

   /**
    * Get the user
    */
   @Test
   public void getUser() {
      String uri = "uriCreated";
      User user = new User("TestUserFound", "testusernotfound@test.it", "TestUserFound");
      user.setId("uniqueId");
      user.setUri(uri);
      // Image image = new Image("TestImage","test");
      // user.addImage(image);
      Mockito.when(userDAOMock.read(user.getUri()))
            .thenReturn(user);
      Response response = target(resourcePath).path(pathGet)
            .path(uri)
            .request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      UserResponse userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(0, userResponse.getErrorCode());
      assertEquals("SUCCESS", userResponse.getStatus());
      User userReceived = userResponse.getUser();
      assertNotNull(userReceived);
      assertEquals(user, userReceived);
   }
}
