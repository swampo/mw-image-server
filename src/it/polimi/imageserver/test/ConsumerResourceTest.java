/**
 *
 */
package it.polimi.imageserver.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider;
import org.glassfish.jersey.server.oauth1.OAuth1ServerFeature;
import org.glassfish.jersey.server.oauth1.OAuth1ServerProperties;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.model.ConsumerBean;
import it.polimi.imageserver.resource.ConsumerResource;

/**
 * Test correct registration of a consumer
 *
 */
public class ConsumerResourceTest extends JerseyTest {
   private static final String REQUEST_TOKEN_URI = "request-token";
   private static final String ACCESS_TOKEN_URI = "access-token";

   private static final String resourcePath = UriBuilder.fromResource(ConsumerResource.class)
         .toString();
   private static final String apiRegistrationPath = "api-register";
   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;

   /*
    * Application configuration for testing
    *
    * (non-Javadoc)
    *
    * @see org.glassfish.jersey.test.JerseyTest#configure()
    */
   @Override
   protected Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      ResourceConfig resconf = new ResourceConfig(ConsumerResource.class);
      resconf.register(MOXyJsonContextResolver.class);
      configureOAuth(resconf);
      return resconf;
   }

   /**
    * Configure oAuth1 feature
    */
   private void configureOAuth(ResourceConfig resconf) {
      /*
       * oAuth 1 Configuration
       */
      // Default OAuth1Provider
      final DefaultOAuth1Provider oAuthProvider = new DefaultOAuth1Provider();
      // OAuth1ServerFeature add to the servlet the oAuth1 support
      // A OAuth1Provider is required
      final OAuth1ServerFeature oAuth1ServerFeature = new OAuth1ServerFeature(oAuthProvider, REQUEST_TOKEN_URI, ACCESS_TOKEN_URI);
      resconf.register(oAuth1ServerFeature);
      // OAuth1 properties
      resconf.property(OAuth1ServerProperties.TIMESTAMP_UNIT, "SECONDS");
      resconf.property(OAuth1ServerProperties.MAX_NONCE_CACHE_SIZE, 20);
      resconf.property(OAuth1ServerProperties.IGNORE_PATH_PATTERN, "authorize/*");
   }

   @Override
   protected void configureClient(ClientConfig clientConfig) {
      clientConfig.register(MOXyJsonContextResolver.class);
   }

   /**
    * Check if the resource is available
    */
   @Test
   public void consumerResourceOnline() {
      Response response = target(resourcePath).request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
   }

   /**
    * Test consumer registration
    */
   @Test
   public void consumerRegistered() {
      String email = "test@test.it";
      Form form = new Form();
      form.param("name", "Test");
      form.param("email", email);
      Response response = target(resourcePath).path(apiRegistrationPath)
            .request(acceptedMediaType)
            .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      ConsumerBean consumer = response.readEntity(ConsumerBean.class);
      assertNotNull(consumer);
      assertEquals(email, consumer.getName());
      assertNotNull(consumer.getKey());
      assertNotNull(consumer.getSecret());
   }

   /**
    * Test consumer registration not valid
    */
   @Test
   public void consumerNotValid() {
      Form form = new Form();
      form.param("name", "aaa");
      form.param("email", "aaa");
      Response response = target(resourcePath).path(apiRegistrationPath)
            .request(acceptedMediaType)
            .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));
      assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
   }
}
