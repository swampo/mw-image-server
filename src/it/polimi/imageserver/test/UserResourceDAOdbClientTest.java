package it.polimi.imageserver.test;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.response.UserResponse;
import it.polimi.imageserver.response.UsersListResponse;

/*
 * RUN AS JAVA APPLICATION
 */
public class UserResourceDAOdbClientTest {

   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;

   private static final String pathResource = "users";
   private static final String pathGet = "info";
   private static final String pathUpdate = "update";
   private static final String pathCreate = "create";
   private static final String pathList = "list";

   public static void main(String[] args) {
      Response r;

      // Client configuration
      ClientConfig clientConfig = new ClientConfig();
      // Register custom MOXyJsonProvider
      clientConfig.register(MOXyJsonContextResolver.class);
      // Register Logging filter
      clientConfig.register(new LoggingFilter(Logger.getLogger("ImageServerLogger"), true));

      // Obtain new client
      Client client = ClientBuilder.newClient(clientConfig);

      // ImageServer target
      WebTarget webTarget = client.target("http://localhost:8080/mw_imageserver/rest")
            .path(pathResource);

      // Test online
      String test = webTarget.request(acceptedMediaType)
            .get(String.class);
      System.out.println(test);


      // Test DATA
      // load test data before with seedDDL.sql

      // Test create
      User uCreate = new User("TestCreate", "testcreate@test.it", "TestCreate");
      r = webTarget.path(pathCreate)
            .request(acceptedMediaType)
            .post(Entity.entity(uCreate, acceptedMediaType));
      if (r.getStatus() != 201) {
         System.out.println("Error: User not created");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      UserResponse ucr = r.readEntity(UserResponse.class);
      System.out.println("User response correctly received");
      if (ucr.getErrorCode() != 0) {
         System.out.println("Error: User not created");
         return;
      }
      uCreate = ucr.getUser();

      // Test get user created
      String uId = uCreate.getId();
      if (uId == null || uId.equals("")) {
         System.out.println("Error: User created id is null");
      }
      System.out.println("User id: " + uId);
      r = webTarget.path(pathGet)
            .path(uId)
            .request(acceptedMediaType)
            .get();
      if (r.getStatus() != 200) {
         System.out.println("Error: User not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      UserResponse ugr = r.readEntity(UserResponse.class);
      System.out.println("User response correctly received");
      if (ugr.getErrorCode() != 0) {
         System.out.println("Error: User not found");
         return;
      }
      User uRead = ugr.getUser();
      System.out.println("[USER] " + uRead);

      // Test update user
      uId = uRead.getId();
      if (uId == null || uId.equals("")) {
         System.out.println("Error: User created id is null");
      }
      System.out.println("User id: " + uId);
      System.out.println("Old name: " + uRead.getName());
      uRead.setName("TestUpdate");
      System.out.println("Set new name: " + uRead.getName());
      r = webTarget.path(pathUpdate)
            .path(uRead.getId())
            .request(acceptedMediaType)
            .put(Entity.entity(uRead, acceptedMediaType));
      if (r.getStatus() != 200) {
         System.out.println("Error: User not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      UserResponse uur = r.readEntity(UserResponse.class);
      System.out.println("User response correctly received");
      if (ugr.getErrorCode() != 0) {
         System.out.println("Error: User not found");
         return;
      }
      User uUpdated = uur.getUser();
      System.out.println("[USER] " + uUpdated);


      // Test get all users
      r = webTarget.path(pathList)
            .request(acceptedMediaType)
            .get();
      if (r.getStatus() != 200) {
         System.out.println("Error: Users list not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      UsersListResponse ulr = r.readEntity(UsersListResponse.class);
      System.out.println("User response correctly received");
      if (ulr.getErrorCode() != 0) {
         System.out.println("Error: Users list not returned");
         return;
      }
      List<User> users = ulr.getUsers();
      for (User u : users) {
         System.out.println(u);
      }

      client.close();
      System.out.println("TEST FINISHED WITHOUT ERRORS");
   }

}
