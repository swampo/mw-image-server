/**
 *
 */
package it.polimi.imageserver.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.security.Principal;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.oauth1.AccessToken;
import org.glassfish.jersey.client.oauth1.ConsumerCredentials;
import org.glassfish.jersey.client.oauth1.OAuth1AuthorizationFlow;
import org.glassfish.jersey.client.oauth1.OAuth1ClientSupport;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider;
import org.glassfish.jersey.server.oauth1.OAuth1ServerFeature;
import org.glassfish.jersey.server.oauth1.OAuth1ServerProperties;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.dao.UserDAOmap;
import it.polimi.imageserver.resource.AuthorizationResource;
import jersey.repackaged.com.google.common.collect.Sets;

/**
 * Test correct registration of a consumer
 *
 */
public class AuthorizationResourceTest extends JerseyTest {
   private static final String REQUEST_TOKEN_ENDPOINT = "request-token";
   private static final String ACCESS_TOKEN_ENDPOINT = "access-token";
   private final URI BASE_URI = getBaseUri();
   private final String tempCredUri = UriBuilder.fromUri(BASE_URI)
         .path(REQUEST_TOKEN_ENDPOINT)
         .build()
         .toString();
   private final String accessTokenUri = UriBuilder.fromUri(BASE_URI)
         .path(ACCESS_TOKEN_ENDPOINT)
         .build()
         .toString();
   private static final String callbackUri = "http://callback";
   // Test Consumer
   private static final String SECRET_CONSUMER_KEY = "secret-consumer-key";
   private static final String CONSUMER_KEY = "my-consumer-key";
   private static final String CONSUMER_NAME = "my-consumer";
   // Default user
   private static final String DEFAULT_EMAIL = "default@default.com";
   private static final String DEFAULT_PASSWORD = "Default";

   private String resourcePath = UriBuilder.fromResource(AuthorizationResource.class)
         .toString();
   private static final String acceptedMediaType = MediaType.TEXT_PLAIN;

   /*
    * Application configuration for testing
    *
    * (non-Javadoc)
    *
    * @see org.glassfish.jersey.test.JerseyTest#configure()
    */
   @Override
   protected Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      ResourceConfig resconf = new ResourceConfig(AuthorizationResource.class);
      resconf.register(MOXyJsonContextResolver.class);
      // DAOs injection
      UserDAO users = new UserDAOmap();
      resconf.register(new AbstractBinder() {
         @Override
         protected void configure() {
            bind(users).to(UserDAO.class);
         }
      });
      configureOAuth(resconf);
      return resconf;
   }

   /**
    * Configure oAuth1 feature
    */
   private void configureOAuth(ResourceConfig resconf) {
      /*
       * oAuth 1 Configuration
       */
      // Default OAuth1Provider
      final DefaultOAuth1Provider oAuthProvider = new DefaultOAuth1Provider();
      // Test Consumer
      oAuthProvider.registerConsumer(CONSUMER_NAME, CONSUMER_KEY, SECRET_CONSUMER_KEY, new MultivaluedHashMap<String, String>());
      // OAuth1ServerFeature add to the servlet the oAuth1 support
      // A OAuth1Provider is required
      final OAuth1ServerFeature oAuth1ServerFeature = new OAuth1ServerFeature(oAuthProvider, REQUEST_TOKEN_ENDPOINT, ACCESS_TOKEN_ENDPOINT);
      resconf.register(oAuth1ServerFeature);
      // OAuth1 properties
      resconf.property(OAuth1ServerProperties.TIMESTAMP_UNIT, "SECONDS");
      resconf.property(OAuth1ServerProperties.MAX_NONCE_CACHE_SIZE, 20);
      resconf.property(OAuth1ServerProperties.IGNORE_PATH_PATTERN, "authorize/*");
   }

   @Override
   protected void configureClient(ClientConfig clientConfig) {
      clientConfig.register(MOXyJsonContextResolver.class);
   }

   /**
    * Check if the resource is available
    */
   @Test
   public void consumerResourceOnline() {
      Response response = target(resourcePath).path("online")
            .request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
   }

   /**
    * Test get verifier with correct user
    */
   @Test
   public void getVerifier() {
      final OAuth1AuthorizationFlow authFlow = OAuth1ClientSupport.builder(new ConsumerCredentials(CONSUMER_KEY, SECRET_CONSUMER_KEY))
            .authorizationFlow(tempCredUri, accessTokenUri, resourcePath)
            .callbackUri(callbackUri)
            .build();
      final String authUri = authFlow.start();
      // ie: authUri: authorize /authorize?oauth_token=ef11cccf1a494d7598f0bae444c0d960
      // Get the token
      String[] split = authUri.split("=");
      String token = split[1];
      // Get authorization
      final Response userAuthResponse = target(resourcePath).queryParam("oauth_token", token)
            .queryParam("user_email", DEFAULT_EMAIL)
            .queryParam("user_password", DEFAULT_PASSWORD)
            .request()
            .get();
      assertEquals(200, userAuthResponse.getStatus());
      assertEquals(true, userAuthResponse.hasEntity());
      String verifier = userAuthResponse.readEntity(String.class);
      assertNotNull(verifier);
      // Get access token
      final AccessToken accessToken = authFlow.finish(verifier);
      assertNotNull(accessToken);
      System.out.println("Request token: " + token);
      System.out.println("Verifier: " + verifier);
      System.out.println("Access token: " + accessToken.getToken());
      System.out.println("Access token secret: " + accessToken.getAccessTokenSecret());
   }

   /**
    * Test get verifier with invalid user
    */
   @Test
   public void userNotValid() {
      final OAuth1AuthorizationFlow authFlow = OAuth1ClientSupport.builder(new ConsumerCredentials(CONSUMER_KEY, SECRET_CONSUMER_KEY))
            .authorizationFlow(tempCredUri, accessTokenUri, resourcePath)
            .callbackUri(callbackUri)
            .build();
      final String authUri = authFlow.start();
      // ie: authUri: authorize /authorize?oauth_token=ef11cccf1a494d7598f0bae444c0d960
      // Get the token
      String[] split = authUri.split("=");
      String token = split[1];
      // Get authorization
      final Response userAuthResponse = target(resourcePath).queryParam("oauth_token", token)
            .queryParam("user_email", "invalid@invalid.it")
            .queryParam("user_password", "invalid")
            .request()
            .get();
      assertEquals(Status.UNAUTHORIZED.getStatusCode(), userAuthResponse.getStatus());
   }

   /**
    * Test get verifier with invalid user
    */
   @Test
   public void nullValues() {
      Response response = target(resourcePath).request(acceptedMediaType)
            .get();
      assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
   }
}
