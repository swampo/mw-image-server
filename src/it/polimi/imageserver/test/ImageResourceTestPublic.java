package it.polimi.imageserver.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.text.ParseException;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.dao.UserDAOmap;
import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.resource.ImageResource;
import it.polimi.imageserver.response.ImageResponse;
import it.polimi.imageserver.response.ImagesListResponse;
import it.polimi.imageserver.response.UserResponse;

/**
 * Test ImageResource
 *
 */
public class ImageResourceTestPublic extends JerseyTest {

   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;
   private String resourcePath = UriBuilder.fromResource(ImageResource.class)
         .toString();
//   private URI baseAppUri = getBaseUri();

   private static final String pathGet = "public/u_default/image";
   private static final String pathGetList = "public/u_default/list";
   private static final String pathUpload = "public/u_default/upload";
   private static final String pathUpdate = "public/u_default/update";

   /*
    * Application configuration for testing
    *
    * (non-Javadoc)
    *
    * @see org.glassfish.jersey.test.JerseyTest#configure()
    */
   @Override
   protected Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      // DAOs injection
      UserDAO users = new UserDAOmap();
      return new ResourceConfig(ImageResource.class).register(new AbstractBinder() {
         @Override
         protected void configure() {
            bind(users).to(UserDAO.class);
         }
      })
            .register(MOXyJsonContextResolver.class)
            .register(MultiPartFeature.class);
   }

   @Override
   protected void configureClient(ClientConfig clientConfig) {
      // enable(TestProperties.LOG_TRAFFIC);
      // enable(TestProperties.DUMP_ENTITY);
      clientConfig.register(MOXyJsonContextResolver.class);
      clientConfig.register(MultiPartFeature.class);
   }

   /**
    * Check if the resource is available
    */
   @Test
   public void userResourceOnline() {
      Response response = target(resourcePath).request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
   }

   /**
    * Test to upload an image
    */
   @Test
   public void uploadImageTest() {
      String absolutePath = System.getProperty("user.dir");
      File fileToUpload = new File(absolutePath + "/test/batman.png");
      FormDataMultiPart multiPart = new FormDataMultiPart();
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      Response response = target(resourcePath).path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      assertNotNull(response.getHeaderString("Location"));
      assertEquals(true, response.hasEntity());
      ImageResponse imageResponse = response.readEntity(ImageResponse.class);
      assertNotNull(imageResponse);
      assertEquals(0, imageResponse.getErrorCode());
      Image image = imageResponse.getImage();
      assertNotNull(image);
//      assertEquals(UriBuilder.fromUri(baseAppUri)
//            .path(image.getUri())
//            .toString(), response.getHeaderString("Location"));
      assertEquals(image.getUri(), response.getHeaderString("Location"));
      List<Image> images = getImagesList();
      assertEquals(false, images.isEmpty());
      int size = images.size();
      // Second upload
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      response = target(resourcePath).path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      images = getImagesList();
      assertEquals(size + 1, images.size());

   }

   /**
    * Update the image info
    */
   @Test
   public void updateImage() {
      // Upload image
      String absolutePath = System.getProperty("user.dir");
      File fileToUpload = new File(absolutePath + "/test/batman.png");
      FormDataMultiPart multiPart = new FormDataMultiPart();
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      Response response = target(resourcePath).path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      assertEquals(true, response.hasEntity());
      ImageResponse imageResponse = response.readEntity(ImageResponse.class);
      assertNotNull(imageResponse);
      assertEquals(0, imageResponse.getErrorCode());
      Image image = imageResponse.getImage();
      assertNotNull(image);
      // Update info
      image.setTitle("New Title");
      response = target(resourcePath).path(pathUpdate)
            .path(image.getId())
            .request(acceptedMediaType)
            .put(Entity.entity(image, acceptedMediaType));
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      imageResponse = response.readEntity(ImageResponse.class);
      assertNotNull(imageResponse);
      assertEquals(0, imageResponse.getErrorCode());
      assertEquals("SUCCESS", imageResponse.getStatus());
      Image imageUpdated = imageResponse.getImage();
      assertNotNull(imageUpdated);
      assertEquals(imageUpdated, image);
      assertEquals(image, imageUpdated);
      assertEquals(image.getTitle(), imageUpdated.getTitle());
   }

   /*
    * Get the user's images list
    */
   private List<Image> getImagesList() {
      Response response;
      ImagesListResponse imagesListResponse;
      response = target(resourcePath).path(pathGetList)
            .request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      imagesListResponse = response.readEntity(ImagesListResponse.class);
      assertNotNull(imagesListResponse);
      assertEquals(0, imagesListResponse.getErrorCode());
      assertEquals("SUCCESS", imagesListResponse.getStatus());
      return imagesListResponse.getImages();
   }

   /**
    * Test to download an image
    */
   @Test
   public void getImageTest() {
      String image = "testDownload";
      String donwloadBasePath = System.getProperty("user.dir") + "/test/download/";
      Response response = target(resourcePath).path(pathGet)
            .path(image)
            .request("image/png")
            .get();
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      String scd = response.getHeaderString("Content-Disposition");
      assertNotNull(scd);
      String fileName = "default.png";
      try {
         ContentDisposition cd = new ContentDisposition(scd);
         fileName = cd.getFileName();
         assertNotNull(fileName);
      } catch (ParseException e) {
         System.out.println("Error parsing content disposition");
         return;
      }
      InputStream inputStream = response.readEntity(InputStream.class);
      // If not exists create directory
      String downloadFilePath = donwloadBasePath + fileName;
      File dir = new File(donwloadBasePath);
      if (!dir.mkdirs()) {
         // TODO throw exception;
      }
      OutputStream outputStream = null;
      try {
         File file = new File(downloadFilePath);
         outputStream = new FileOutputStream(file);
         int read = 0;
         byte[] bytes = new byte[1024]; // 16384
         while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
         }
         outputStream.flush();
      } catch (IOException ioe) {
         ioe.printStackTrace();
      } finally {
         // release resource, if any
         try {
            outputStream.close();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }

   /**
    * Test image not found
    */
   @Test
   public void getImageNotFoundTest() {
      String image = "xyz.png";
      Response response = target(resourcePath).path(pathGet)
            .path(image)
            .request("image/png")
            .get();
      assertEquals(404, response.getStatus());
      assertEquals(true, response.hasEntity());
      ImageResponse errorResponse = response.readEntity(ImageResponse.class);
      assertNotNull(errorResponse);
      assertEquals(4054, errorResponse.getErrorCode());
      assertEquals("FAIL", errorResponse.getStatus());
   }
}
