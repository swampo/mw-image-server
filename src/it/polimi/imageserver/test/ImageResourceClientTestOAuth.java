package it.polimi.imageserver.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.oauth1.AccessToken;
import org.glassfish.jersey.client.oauth1.ConsumerCredentials;
import org.glassfish.jersey.client.oauth1.OAuth1AuthorizationFlow;
import org.glassfish.jersey.client.oauth1.OAuth1ClientSupport;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.model.ConsumerBean;
import it.polimi.imageserver.model.Image;
import it.polimi.imageserver.response.ImageResponse;
import it.polimi.imageserver.response.ImagesListResponse;

/*
 * RUN AS JAVA APPLICATION
 */
public class ImageResourceClientTestOAuth {
   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;

   private static final String BASE_URI = "http://localhost:8080/mw_imageserver/rest";
   private static final String IMAGE_RESOURCE = "images";
   private static final String REQUEST_TOKEN_ENDPOINT = "request-token";
   private static final String ACCESS_TOKEN_ENDPOINT = "access-token";
   private static final String AUTHORIZE_ENDPOINT = "authorize";
   private static final String tempCredUri = UriBuilder.fromUri(BASE_URI)
         .path(REQUEST_TOKEN_ENDPOINT)
         .build()
         .toString();
   private static final String accessTokenUri = UriBuilder.fromUri(BASE_URI)
         .path(ACCESS_TOKEN_ENDPOINT)
         .build()
         .toString();
   private static final String userAuthorizationUri = UriBuilder.fromUri(BASE_URI)
         .path(AUTHORIZE_ENDPOINT)
         .build()
         .toString();
   // User for testing: u_default
   private static final String pathGet = "u_default/image";
   private static final String pathGetList = "u_default/list";
   private static final String pathUpload = "u_default/upload";
   private static final String pathUpdate = "u_default/update";

   public static void main(String[] args) {
      Response r;

      // Client configuration
      ClientConfig clientConfig = new ClientConfig();
      // Register custom MOXyJsonProvider
      clientConfig.register(MOXyJsonContextResolver.class);
      // Register Logging filter
      clientConfig.register(new LoggingFilter(Logger.getLogger("ImageServerLogger"), true));
      // Multipart
      clientConfig.register(MultiPartFeature.class);

      // Obtain new client
      Client client = ClientBuilder.newClient(clientConfig);
      // Get unauthorized client
      WebTarget webTarget = client.target(BASE_URI);

      // Test unauthorized
      r = webTarget.path(IMAGE_RESOURCE)
            .request(acceptedMediaType)
            .get();
      if (r.getStatus() != 403) {
         System.out.println("Error: 403 forbidden expected");
         return;
      }

      // Register new consumer
      Form form = new Form();
      form.param("name", "Test");
      form.param("email", "test@test.it");
      r = webTarget.path("consumer")
            .path("api-register")
            .request(acceptedMediaType)
            .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));
      if (r.getStatus() != 200) {
         System.out.println("Error: Consumer not registered");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      ConsumerBean cb = r.readEntity(ConsumerBean.class);
      System.out.println("Consumer correctly registered:");
      System.out.println(cb.toString());

      // #### GET AUTHORIZED CLIENT
      String callbackUri = "http://callback";
      final OAuth1AuthorizationFlow authFlow = OAuth1ClientSupport.builder(new ConsumerCredentials(cb.getKey(), cb.getSecret()))
            .authorizationFlow(tempCredUri, accessTokenUri, userAuthorizationUri)
            .callbackUri(callbackUri)
            .build();
      final String authUri = authFlow.start();
      // Unauthorized user
      r = client.target(authUri)
            .queryParam("user_email", "invalid@default.it")
            .queryParam("user_password", "invalid")
            .request()
            .get();
      if(r.getStatus() != 401){
         System.out.println("Error: expected unauthorized status");
         return;
      }
      // Authorize Default user
      r = client.target(authUri)
            .queryParam("user_email", "default@test.com")
            .queryParam("user_password", "Default")
            .request()
            .get();
      if (!(r.getStatus() == 200)) {
         System.out.println("Something goes wrong during authorization");
         return;
      }
      final String verifier = r.readEntity(String.class);
      System.out.println("Verifier: " + verifier);
      final AccessToken accessToken = authFlow.finish(verifier);
      System.out.println("Access token: " + accessToken.getToken() + " , " + accessToken.getAccessTokenSecret());
//    client = authFlow.getAuthorizedClient(); //This return a new client authorized using the accessToken of the flow
      // Make the old client already configured authorized
      final Feature filterFeature = OAuth1ClientSupport.builder(new ConsumerCredentials(cb.getKey(), cb.getSecret()))
            .feature()
            .accessToken(accessToken)
            .build();
      client.register(filterFeature);

      webTarget = client.target(BASE_URI).path(IMAGE_RESOURCE);

      System.out.println("AUTHORIZED CLIENT");

      // Test online
      String test = webTarget.request(acceptedMediaType)
            .get(String.class);
      System.out.println(test);

      // Test upload
      String absolutePath = System.getProperty("user.dir");
      File fileToUpload = new File(absolutePath + "/test/batman.png");
      FormDataMultiPart multiPart = new FormDataMultiPart();
      if (fileToUpload != null) {
         multiPart.bodyPart(new FileDataBodyPart("file", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      }
      r = webTarget.path(pathUpload)
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE));
      if (r.getStatus() != 201) {
         System.out.println("Error: Image not uploaded");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      ImageResponse iur = r.readEntity(ImageResponse.class);
      System.out.println("Image response correctly received");
      if (iur.getErrorCode() != 0) {
         System.out.println("Error: Image not uploaded");
         return;
      }
      Image image = iur.getImage();
      System.out.println("[IMAGE] " + image.toString());

      // Test Update
      System.out.println("Old title: " + image.getTitle());
      image.setTitle("il_cavaliere_oscuro");
      System.out.println("Set new title: " + image.getTitle());
      r = webTarget.path(pathUpdate)
            .path(image.getId())
            .request(acceptedMediaType)
            .put(Entity.entity(image, acceptedMediaType));
      if (r.getStatus() != 200) {
         System.out.println("Error: Image not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      ImageResponse iupr = r.readEntity(ImageResponse.class);
      System.out.println("User response correctly received");
      if (iupr.getErrorCode() != 0) {
         System.out.println("Error: Image not found");
         return;
      }
      image = iupr.getImage();
      System.out.println("[IMAGE] " + image.toString());

      // Test get images list
      r = webTarget.path(pathGetList)
            .request(acceptedMediaType)
            .get();
      if (r.getStatus() != 200) {
         System.out.println("Error: Image list not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      ImagesListResponse ilr = r.readEntity(ImagesListResponse.class);
      System.out.println("User response correctly received");
      if (ilr.getErrorCode() != 0) {
         System.out.println("Error: Images list not returned");
         return;
      }
      List<Image> images = ilr.getImages();
      for (Image i : images) {
         System.out.println(i);
      }

      // Test download
      // NOTE: when starts glassfish erase the directory. Reupload testDownload
      String donwloadBasePath = System.getProperty("user.dir") + "/test/download_deploy/";
      r = webTarget.path(pathGet)
            .path("testDownload")
            .request("image/png")
            .get();
      if (r.getStatus() != 200) {
         System.out.println("Error: Image not received");
         return;
      }
      if (!r.hasEntity()) {
         System.out.println("Error: Response received but without entity");
         return;
      }
      String scd = r.getHeaderString("Content-Disposition");
      String fileName = "default.png";
      if (scd != null) {
         try {
            ContentDisposition cd = new ContentDisposition(scd);
            fileName = cd.getFileName();
         } catch (ParseException e) {
            System.out.println("Error parsing content disposition");
            return;
         }
      }
      InputStream inputStream = r.readEntity(InputStream.class);
      // If not exists create directory
      String downloadFilePath = donwloadBasePath + fileName;
      File dir = new File(donwloadBasePath);
      if (!dir.mkdirs()) {
         // TODO throw exception;
      }
      OutputStream outputStream = null;
      try {
         File file = new File(downloadFilePath);
         outputStream = new FileOutputStream(file);
         int read = 0;
         byte[] bytes = new byte[1024]; // 16384
         while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
         }
         outputStream.flush();
         System.out.println("Image downloaded!");
      } catch (IOException ioe) {
         ioe.printStackTrace();
      } finally {
         // release resource, if any
         try {
            outputStream.close();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }

      client.close();
      System.out.println("TEST FINISHED WITHOUT ERRORS");
   }

}
