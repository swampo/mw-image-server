package it.polimi.imageserver.test;

import static org.junit.Assert.*;

import java.net.URI;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import it.polimi.imageserver.config.MOXyJsonContextResolver;
import it.polimi.imageserver.dao.UserDAO;
import it.polimi.imageserver.dao.UserDAOmap;
import it.polimi.imageserver.model.User;
import it.polimi.imageserver.resource.UserResource;
import it.polimi.imageserver.response.UserResponse;
import it.polimi.imageserver.response.UsersListResponse;

/**
 * Test for UserResource with a real UsersDAO implementation (UserDAOmap)
 *
 */
public class UserResourceDAOmapTest extends JerseyTest {

   private static final String acceptedMediaType = MediaType.APPLICATION_JSON;
   private static final String resourcePath = UriBuilder.fromResource(UserResource.class)
         .toString();
   private URI baseAppUri = getBaseUri();

   private static final String pathGet = "info";
   private static final String pathCreate = "create";
   private static final String pathUpdate = "update";
   private static final String pathList = "list";

   /*
    * Application configuration for testing
    *
    * (non-Javadoc)
    *
    * @see org.glassfish.jersey.test.JerseyTest#configure()
    */
   @Override
   protected Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      // DAOs injection
      UserDAO users = new UserDAOmap();
      return new ResourceConfig(UserResource.class).register(new AbstractBinder() {
         @Override
         protected void configure() {
            bind(users).to(UserDAO.class);
         }
      })
            .register(MOXyJsonContextResolver.class);
   }

   @Override
   protected void configureClient(ClientConfig clientConfig) {
      // Register custom MOXyJsonProvider
      clientConfig.register(MOXyJsonContextResolver.class);
   }

   /**
    * Check if the resource is available
    */
   @Test
   public void userResourceOnline() {
      Response response = target(resourcePath).request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
   }

   /**
    * Create an user
    */
   @Test
   public void createUser() {
      User user = new User("TestCreate", "testcreate@test.it", "TestCreate");
      Response response = target(resourcePath).path(pathCreate)
            .request(acceptedMediaType)
            .post(Entity.entity(user, acceptedMediaType));
      // 201 new resource created
      assertEquals(201, response.getStatus());
      assertNotNull(response.getHeaderString("Location"));
      assertEquals(true, response.hasEntity());
      UserResponse userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(0, userResponse.getErrorCode());
      User receivedUser = userResponse.getUser();
      assertNotNull(receivedUser);
      assertEquals(receivedUser.getUri(), response.getHeaderString("Location"));
   }

   /**
    * User not found
    */
   @Test
   public void userNotFound() {
      String id = "uriNotCreated";
      Response response = target(resourcePath).path(pathGet)
            .path(id)
            .request(acceptedMediaType)
            .get();
      // 404 resource not found
      assertEquals(404, response.getStatus());
      assertEquals(true, response.hasEntity());
      UserResponse userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(4004, userResponse.getErrorCode());
      assertEquals("FAIL", userResponse.getStatus());
      User userReceived = userResponse.getUser();
      assertNull(userReceived);
   }

   /**
    * Get the user
    */
   @Test
   public void getUser() {
      // User created before in UserDAOmap
      String userId = "u_user0";
      Response response;
      UserResponse userResponse;
      response = target(resourcePath).path(pathGet)
            .path(userId)
            .request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(0, userResponse.getErrorCode());
      assertEquals("SUCCESS", userResponse.getStatus());
      User userReceived = userResponse.getUser();
      assertNotNull(userReceived);
   }

   /**
    * Update the user
    */
   @Test
   public void updateUser() {
      // User created before in UserDAOmap
      String userId = "u_user2";
      Response response;
      UserResponse userResponse;
      response = target(resourcePath).path(pathGet)
            .path(userId)
            .request(acceptedMediaType)
            .get();
      userResponse = response.readEntity(UserResponse.class);
      User userReceived = userResponse.getUser();
      // New user values
      userReceived.setName("NewName2");
      response = target(resourcePath).path(pathUpdate)
            .path(userId)
            .request(acceptedMediaType)
            .put(Entity.entity(userReceived, acceptedMediaType));
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      userResponse = response.readEntity(UserResponse.class);
      assertNotNull(userResponse);
      assertEquals(0, userResponse.getErrorCode());
      assertEquals("SUCCESS", userResponse.getStatus());
      User userUpdated = userResponse.getUser();
      assertNotNull(userUpdated);
      assertEquals(userUpdated, userReceived);
      assertEquals(userReceived, userUpdated);
      assertEquals(userReceived.getName(), userUpdated.getName());
   }

   /**
    * Get list of users
    */
   @Test
   public void getAllUsers() {
      Response response = target(resourcePath).path(pathList)
            .request(acceptedMediaType)
            .get();
      assertEquals(200, response.getStatus());
      assertEquals(true, response.hasEntity());
      UsersListResponse usersListResponse = response.readEntity(UsersListResponse.class);
      assertNotNull(usersListResponse);
      assertEquals("SUCCESS", usersListResponse.getStatus());
   }
}
