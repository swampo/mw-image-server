INSERT INTO `USER` (`ID`, `created`, `email`, `lastUpdate`, `name`, `password`, `uri`) VALUES
('u_default', '2016-04-26 13:27:01', 'default@test.com', '2016-04-26 13:27:01', 'Default', 'Default', '/users/info/u_default'),
('u_user0', '2016-04-26 13:27:01', 'user0@test.com', '2016-04-26 13:27:01', 'User0', 'User0', '/users/info/u_user0'),
('u_user1', '2016-04-26 13:27:01', 'user1@test.com', '2016-04-26 13:27:01', 'User1', 'User1', '/users/info/u_user1'),
('u_user2', '2016-04-26 13:27:01', 'user2@test.com', '2016-04-26 13:27:01', 'User2', 'User2', '/users/info/u_user2'),
('u_user3', '2016-04-26 13:27:01', 'user3@test.com', '2016-04-26 13:27:01', 'User3', 'User3', '/users/info/u_user3'),
('u_user4', '2016-04-26 13:27:01', 'user4@test.com', '2016-04-26 13:27:01', 'User4', 'User4', '/users/info/u_user4');

INSERT INTO `IMAGE` (`ID`, `caption`, `created`, `extension`, `lastUpdate`, `title`, `uri`, `OWNER_ID`, `IMAGES_KEY`) VALUES
('testDownload', NULL, '2016-04-26 17:50:38', 'png', '2016-04-26 17:50:38', 'batman', '/images/u_default/image/testDownload', 'u_default', 'testDownload');
