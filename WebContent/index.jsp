<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>Simple test client</title>
  </head>
  <body>
    <h1>Create new user</h1>
	<form action="http://localhost:8080/ImageServer/rest/users/" method="post" target="_blank">
	   Name: <input type="text" name="name" /><br/>
	   <input type="submit" value="Create" />
	</form>
	<hr/>
    <h1>Upload new image</h1>
    <script type="text/javascript">
	function sendImage() {
    	var userId = this.document.image.userId.value;
    	this.document.image.action = "http://localhost:8080/ImageServer/rest/users/"+userId+"/imageContainer/"
 		this.document.image.submit();
	}
    </script>
	<form name="image" action="#" method="post" enctype="multipart/form-data" target="_blank">
	   UserId: <input type="text" name="userId" />
	   <p>
		Select a file : <input type="file" name="file" size="50" />
	   </p>
	   Title: <input type="text" name="imageTitle" />
	   <input type="button" value="Upload It" onclick="sendImage()" />
	</form>
  </body>
</html> 
