#MW_ImageServer

##DEPLOY / TESTING
Change UserDAO injection in: UserResource, ImageResource, AuthorizationResource, ImageServerConfig

##TEST MODE
Test mode require that @ Injection annotation is used in Resource classes to inject UserDAO object.
In test mode UserDAO store users in a HashMap.
Under the package "it.polimi.imageserver.test" there are the test to launch as JUnit test.

##DEPLOY MODE
Deploy on Glassfish require that @ EJB annotation is used in Resource classes to inject UserDAO object.
In deploy UserDAO store users into db using JPA.

##DB INITIALIZATION
1) Right click project -> JPA Tools -> Generate Tables from Entities...
NB: generate both db and script sql. Olds sql files are under DDLs folder
2) Run from DDLs folder the script seedDDL.sql (Right click seedDDL.sql -> Execute SQL files...)
3) Check tables are created correctly